package androidlib

import (
	"math"
	"strings"

	"git.wow.st/gmp/jni"
)

var _ = math.Float32bits
var _ = strings.HasPrefix

type Java_lang_ClassLoaderClass struct {
	vmc   *VMContext
	Class jni.Class
}

type Java_lang_ClassLoaderInstance struct {
	*Java_lang_ClassLoaderClass
	Jobj jni.Object
}

func Java_lang_ClassLoader(vmc *VMContext) *Java_lang_ClassLoaderClass {
	return &Java_lang_ClassLoaderClass{
		vmc:   vmc,
		Class: jni.FindClass(vmc.JNIEnv, "java/lang/ClassLoader"),
	}
}

func Java_lang_ClassLoader_Instance(vmc *VMContext, jobj jni.Object) *Java_lang_ClassLoaderInstance {
	return &Java_lang_ClassLoaderInstance{
		Java_lang_ClassLoaderClass: Java_lang_ClassLoader(vmc),
		Jobj: jobj,
	}
}

func (e *Java_lang_ClassLoaderInstance) ClearAssertionStatus() (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "clearAssertionStatus", "()V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Java_lang_ClassLoaderInstance) GetParent() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getParent", "()Ljava/lang/ClassLoader;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Java_lang_ClassLoaderInstance) GetResource(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getResource", "(Ljava/lang/String;)Ljava/net/URL;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Java_lang_ClassLoaderInstance) GetResourceAsStream(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getResourceAsStream", "(Ljava/lang/String;)Ljava/io/InputStream;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Java_lang_ClassLoaderInstance) GetResources(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getResources", "(Ljava/lang/String;)Ljava/util/Enumeration;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Java_lang_ClassLoaderClass) GetSystemClassLoader() (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "getSystemClassLoader", "()Ljava/lang/ClassLoader;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id)
}

func (e *Java_lang_ClassLoaderClass) GetSystemResource(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "getSystemResource", "(Ljava/lang/String;)Ljava/net/URL;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(arg0)))
}

func (e *Java_lang_ClassLoaderClass) GetSystemResourceAsStream(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "getSystemResourceAsStream", "(Ljava/lang/String;)Ljava/io/InputStream;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(arg0)))
}

func (e *Java_lang_ClassLoaderClass) GetSystemResources(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "getSystemResources", "(Ljava/lang/String;)Ljava/util/Enumeration;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(arg0)))
}

func (e *Java_lang_ClassLoaderInstance) LoadClass(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "loadClass", "(Ljava/lang/String;)Ljava/lang/Class;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Java_lang_ClassLoaderInstance) SetClassAssertionStatus(arg0 jni.Object, arg1 bool) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setClassAssertionStatus", "(Ljava/lang/String;Z)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(Boole(arg1)))
}

func (e *Java_lang_ClassLoaderInstance) SetDefaultAssertionStatus(arg0 bool) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setDefaultAssertionStatus", "(Z)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(Boole(arg0)))
}

func (e *Java_lang_ClassLoaderInstance) SetPackageAssertionStatus(arg0 jni.Object, arg1 bool) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setPackageAssertionStatus", "(Ljava/lang/String;Z)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(Boole(arg1)))
}

