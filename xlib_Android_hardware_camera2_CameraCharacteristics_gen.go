package androidlib

import (
	"math"
	"strings"

	"git.wow.st/gmp/jni"
)

var _ = math.Float32bits
var _ = strings.HasPrefix

type Android_hardware_camera2_CameraCharacteristicsClass struct {
	vmc   *VMContext
	Class jni.Class
}

type Android_hardware_camera2_CameraCharacteristicsInstance struct {
	*Android_hardware_camera2_CameraCharacteristicsClass
	Jobj jni.Object
}

func Android_hardware_camera2_CameraCharacteristics(vmc *VMContext) *Android_hardware_camera2_CameraCharacteristicsClass {
	return &Android_hardware_camera2_CameraCharacteristicsClass{
		vmc:   vmc,
		Class: jni.FindClass(vmc.JNIEnv, "android/hardware/camera2/CameraCharacteristics"),
	}
}

func Android_hardware_camera2_CameraCharacteristics_Instance(vmc *VMContext, jobj jni.Object) *Android_hardware_camera2_CameraCharacteristicsInstance {
	return &Android_hardware_camera2_CameraCharacteristicsInstance{
		Android_hardware_camera2_CameraCharacteristicsClass: Android_hardware_camera2_CameraCharacteristics(vmc),
		Jobj: jobj,
	}
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) COLOR_CORRECTION_AVAILABLE_ABERRATION_MODES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "COLOR_CORRECTION_AVAILABLE_ABERRATION_MODES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) CONTROL_AE_AVAILABLE_ANTIBANDING_MODES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_AVAILABLE_ANTIBANDING_MODES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) CONTROL_AE_AVAILABLE_MODES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_AVAILABLE_MODES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) CONTROL_AE_COMPENSATION_RANGE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_COMPENSATION_RANGE", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) CONTROL_AE_COMPENSATION_STEP() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_COMPENSATION_STEP", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) CONTROL_AE_LOCK_AVAILABLE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_LOCK_AVAILABLE", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) CONTROL_AF_AVAILABLE_MODES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AF_AVAILABLE_MODES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) CONTROL_AVAILABLE_EFFECTS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AVAILABLE_EFFECTS", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) CONTROL_AVAILABLE_EXTENDED_SCENE_MODE_CAPABILITIES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AVAILABLE_EXTENDED_SCENE_MODE_CAPABILITIES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) CONTROL_AVAILABLE_MODES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AVAILABLE_MODES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) CONTROL_AVAILABLE_SCENE_MODES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AVAILABLE_SCENE_MODES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) CONTROL_AVAILABLE_VIDEO_STABILIZATION_MODES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AVAILABLE_VIDEO_STABILIZATION_MODES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) CONTROL_AWB_AVAILABLE_MODES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AWB_AVAILABLE_MODES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) CONTROL_AWB_LOCK_AVAILABLE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AWB_LOCK_AVAILABLE", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) CONTROL_MAX_REGIONS_AE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_MAX_REGIONS_AE", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) CONTROL_MAX_REGIONS_AF() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_MAX_REGIONS_AF", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) CONTROL_MAX_REGIONS_AWB() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_MAX_REGIONS_AWB", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) CONTROL_POST_RAW_SENSITIVITY_BOOST_RANGE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_POST_RAW_SENSITIVITY_BOOST_RANGE", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) CONTROL_ZOOM_RATIO_RANGE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_ZOOM_RATIO_RANGE", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) DEPTH_DEPTH_IS_EXCLUSIVE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DEPTH_DEPTH_IS_EXCLUSIVE", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) DISTORTION_CORRECTION_AVAILABLE_MODES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DISTORTION_CORRECTION_AVAILABLE_MODES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) EDGE_AVAILABLE_EDGE_MODES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EDGE_AVAILABLE_EDGE_MODES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) FLASH_INFO_AVAILABLE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLASH_INFO_AVAILABLE", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) HOT_PIXEL_AVAILABLE_HOT_PIXEL_MODES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "HOT_PIXEL_AVAILABLE_HOT_PIXEL_MODES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) INFO_DEVICE_STATE_SENSOR_ORIENTATION_MAP() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "INFO_DEVICE_STATE_SENSOR_ORIENTATION_MAP", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) INFO_SUPPORTED_HARDWARE_LEVEL() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "INFO_SUPPORTED_HARDWARE_LEVEL", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) INFO_VERSION() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "INFO_VERSION", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) JPEG_AVAILABLE_THUMBNAIL_SIZES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "JPEG_AVAILABLE_THUMBNAIL_SIZES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) LENS_DISTORTION() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_DISTORTION", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) LENS_DISTORTION_MAXIMUM_RESOLUTION() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_DISTORTION_MAXIMUM_RESOLUTION", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) LENS_FACING() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_FACING", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) LENS_INFO_AVAILABLE_APERTURES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_INFO_AVAILABLE_APERTURES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) LENS_INFO_AVAILABLE_FILTER_DENSITIES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_INFO_AVAILABLE_FILTER_DENSITIES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) LENS_INFO_AVAILABLE_FOCAL_LENGTHS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_INFO_AVAILABLE_FOCAL_LENGTHS", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) LENS_INFO_AVAILABLE_OPTICAL_STABILIZATION() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_INFO_AVAILABLE_OPTICAL_STABILIZATION", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) LENS_INFO_FOCUS_DISTANCE_CALIBRATION() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_INFO_FOCUS_DISTANCE_CALIBRATION", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) LENS_INFO_HYPERFOCAL_DISTANCE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_INFO_HYPERFOCAL_DISTANCE", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) LENS_INFO_MINIMUM_FOCUS_DISTANCE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_INFO_MINIMUM_FOCUS_DISTANCE", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) LENS_INTRINSIC_CALIBRATION() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_INTRINSIC_CALIBRATION", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) LENS_INTRINSIC_CALIBRATION_MAXIMUM_RESOLUTION() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_INTRINSIC_CALIBRATION_MAXIMUM_RESOLUTION", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) LENS_POSE_REFERENCE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_POSE_REFERENCE", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) LENS_POSE_ROTATION() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_POSE_ROTATION", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) LENS_POSE_TRANSLATION() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_POSE_TRANSLATION", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) LENS_RADIAL_DISTORTION() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_RADIAL_DISTORTION", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) LOGICAL_MULTI_CAMERA_SENSOR_SYNC_TYPE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LOGICAL_MULTI_CAMERA_SENSOR_SYNC_TYPE", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) NOISE_REDUCTION_AVAILABLE_NOISE_REDUCTION_MODES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "NOISE_REDUCTION_AVAILABLE_NOISE_REDUCTION_MODES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) REPROCESS_MAX_CAPTURE_STALL() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REPROCESS_MAX_CAPTURE_STALL", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) REQUEST_AVAILABLE_CAPABILITIES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_AVAILABLE_CAPABILITIES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) REQUEST_MAX_NUM_INPUT_STREAMS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_MAX_NUM_INPUT_STREAMS", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) REQUEST_MAX_NUM_OUTPUT_PROC() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_MAX_NUM_OUTPUT_PROC", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) REQUEST_MAX_NUM_OUTPUT_PROC_STALLING() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_MAX_NUM_OUTPUT_PROC_STALLING", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) REQUEST_MAX_NUM_OUTPUT_RAW() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_MAX_NUM_OUTPUT_RAW", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) REQUEST_PARTIAL_RESULT_COUNT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_PARTIAL_RESULT_COUNT", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) REQUEST_PIPELINE_MAX_DEPTH() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_PIPELINE_MAX_DEPTH", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SCALER_AVAILABLE_MAX_DIGITAL_ZOOM() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SCALER_AVAILABLE_MAX_DIGITAL_ZOOM", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SCALER_AVAILABLE_ROTATE_AND_CROP_MODES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SCALER_AVAILABLE_ROTATE_AND_CROP_MODES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SCALER_CROPPING_TYPE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SCALER_CROPPING_TYPE", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SCALER_DEFAULT_SECURE_IMAGE_SIZE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SCALER_DEFAULT_SECURE_IMAGE_SIZE", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SCALER_MANDATORY_CONCURRENT_STREAM_COMBINATIONS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SCALER_MANDATORY_CONCURRENT_STREAM_COMBINATIONS", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SCALER_MANDATORY_MAXIMUM_RESOLUTION_STREAM_COMBINATIONS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SCALER_MANDATORY_MAXIMUM_RESOLUTION_STREAM_COMBINATIONS", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SCALER_MANDATORY_STREAM_COMBINATIONS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SCALER_MANDATORY_STREAM_COMBINATIONS", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SCALER_MULTI_RESOLUTION_STREAM_CONFIGURATION_MAP() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SCALER_MULTI_RESOLUTION_STREAM_CONFIGURATION_MAP", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SCALER_STREAM_CONFIGURATION_MAP() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SCALER_STREAM_CONFIGURATION_MAP", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SCALER_STREAM_CONFIGURATION_MAP_MAXIMUM_RESOLUTION() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SCALER_STREAM_CONFIGURATION_MAP_MAXIMUM_RESOLUTION", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_AVAILABLE_TEST_PATTERN_MODES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_AVAILABLE_TEST_PATTERN_MODES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_BLACK_LEVEL_PATTERN() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_BLACK_LEVEL_PATTERN", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_CALIBRATION_TRANSFORM1() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_CALIBRATION_TRANSFORM1", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_CALIBRATION_TRANSFORM2() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_CALIBRATION_TRANSFORM2", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_COLOR_TRANSFORM1() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_COLOR_TRANSFORM1", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_COLOR_TRANSFORM2() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_COLOR_TRANSFORM2", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_FORWARD_MATRIX1() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_FORWARD_MATRIX1", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_FORWARD_MATRIX2() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_FORWARD_MATRIX2", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_INFO_ACTIVE_ARRAY_SIZE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_INFO_ACTIVE_ARRAY_SIZE", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_INFO_ACTIVE_ARRAY_SIZE_MAXIMUM_RESOLUTION() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_INFO_ACTIVE_ARRAY_SIZE_MAXIMUM_RESOLUTION", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_INFO_BINNING_FACTOR() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_INFO_BINNING_FACTOR", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_INFO_COLOR_FILTER_ARRANGEMENT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_INFO_COLOR_FILTER_ARRANGEMENT", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_INFO_EXPOSURE_TIME_RANGE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_INFO_EXPOSURE_TIME_RANGE", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_INFO_LENS_SHADING_APPLIED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_INFO_LENS_SHADING_APPLIED", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_INFO_MAX_FRAME_DURATION() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_INFO_MAX_FRAME_DURATION", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_INFO_PHYSICAL_SIZE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_INFO_PHYSICAL_SIZE", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_INFO_PIXEL_ARRAY_SIZE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_INFO_PIXEL_ARRAY_SIZE", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_INFO_PIXEL_ARRAY_SIZE_MAXIMUM_RESOLUTION() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_INFO_PIXEL_ARRAY_SIZE_MAXIMUM_RESOLUTION", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_INFO_PRE_CORRECTION_ACTIVE_ARRAY_SIZE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_INFO_PRE_CORRECTION_ACTIVE_ARRAY_SIZE", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_INFO_PRE_CORRECTION_ACTIVE_ARRAY_SIZE_MAXIMUM_RESOLUTION() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_INFO_PRE_CORRECTION_ACTIVE_ARRAY_SIZE_MAXIMUM_RESOLUTION", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_INFO_SENSITIVITY_RANGE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_INFO_SENSITIVITY_RANGE", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_INFO_TIMESTAMP_SOURCE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_INFO_TIMESTAMP_SOURCE", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_INFO_WHITE_LEVEL() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_INFO_WHITE_LEVEL", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_MAX_ANALOG_SENSITIVITY() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_MAX_ANALOG_SENSITIVITY", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_OPTICAL_BLACK_REGIONS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_OPTICAL_BLACK_REGIONS", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_ORIENTATION() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_ORIENTATION", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_REFERENCE_ILLUMINANT1() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_REFERENCE_ILLUMINANT1", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SENSOR_REFERENCE_ILLUMINANT2() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_REFERENCE_ILLUMINANT2", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SHADING_AVAILABLE_MODES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SHADING_AVAILABLE_MODES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) STATISTICS_INFO_AVAILABLE_FACE_DETECT_MODES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "STATISTICS_INFO_AVAILABLE_FACE_DETECT_MODES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) STATISTICS_INFO_AVAILABLE_HOT_PIXEL_MAP_MODES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "STATISTICS_INFO_AVAILABLE_HOT_PIXEL_MAP_MODES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) STATISTICS_INFO_AVAILABLE_LENS_SHADING_MAP_MODES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "STATISTICS_INFO_AVAILABLE_LENS_SHADING_MAP_MODES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) STATISTICS_INFO_AVAILABLE_OIS_DATA_MODES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "STATISTICS_INFO_AVAILABLE_OIS_DATA_MODES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) STATISTICS_INFO_MAX_FACE_COUNT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "STATISTICS_INFO_MAX_FACE_COUNT", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) SYNC_MAX_LATENCY() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SYNC_MAX_LATENCY", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) TONEMAP_AVAILABLE_TONE_MAP_MODES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "TONEMAP_AVAILABLE_TONE_MAP_MODES", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsClass) TONEMAP_MAX_CURVE_POINTS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "TONEMAP_MAX_CURVE_POINTS", "Landroid/hardware/camera2/CameraCharacteristics$Key;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsInstance) Get(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "get", "(Landroid/hardware/camera2/CameraCharacteristics$Key;)Ljava/lang/Object;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_hardware_camera2_CameraCharacteristicsInstance) GetAvailableCaptureRequestKeys() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getAvailableCaptureRequestKeys", "()Ljava/util/List;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsInstance) GetAvailableCaptureResultKeys() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getAvailableCaptureResultKeys", "()Ljava/util/List;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsInstance) GetAvailablePhysicalCameraRequestKeys() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getAvailablePhysicalCameraRequestKeys", "()Ljava/util/List;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsInstance) GetAvailableSessionKeys() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getAvailableSessionKeys", "()Ljava/util/List;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsInstance) GetKeys() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getKeys", "()Ljava/util/List;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsInstance) GetKeysNeedingPermission() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getKeysNeedingPermission", "()Ljava/util/List;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsInstance) GetPhysicalCameraIds() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getPhysicalCameraIds", "()Ljava/util/Set;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_hardware_camera2_CameraCharacteristicsInstance) GetRecommendedStreamConfigurationMap(arg0 int32) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getRecommendedStreamConfigurationMap", "(I)Landroid/hardware/camera2/params/RecommendedStreamConfigurationMap;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((arg0)))
}

