package androidlib

import (
	"math"
	"strings"

	"git.wow.st/gmp/jni"
)

var _ = math.Float32bits
var _ = strings.HasPrefix

type Android_content_ContextClass struct {
	vmc   *VMContext
	Class jni.Class
}

type Android_content_ContextInstance struct {
	*Android_content_ContextClass
	Jobj jni.Object
}

func Android_content_Context(vmc *VMContext) *Android_content_ContextClass {
	return &Android_content_ContextClass{
		vmc:   vmc,
		Class: jni.FindClass(vmc.JNIEnv, "android/content/Context"),
	}
}

func Android_content_Context_Instance(vmc *VMContext, jobj jni.Object) *Android_content_ContextInstance {
	return &Android_content_ContextInstance{
		Android_content_ContextClass: Android_content_Context(vmc),
		Jobj: jobj,
	}
}

func (e *Android_content_ContextClass) ACCESSIBILITY_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACCESSIBILITY_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) ACCOUNT_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACCOUNT_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) ACTIVITY_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTIVITY_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) ALARM_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ALARM_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) APPWIDGET_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "APPWIDGET_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) APP_OPS_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "APP_OPS_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) APP_SEARCH_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "APP_SEARCH_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) AUDIO_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "AUDIO_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) BATTERY_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "BATTERY_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) BIND_ABOVE_CLIENT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "BIND_ABOVE_CLIENT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) BIND_ADJUST_WITH_ACTIVITY() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "BIND_ADJUST_WITH_ACTIVITY", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) BIND_ALLOW_OOM_MANAGEMENT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "BIND_ALLOW_OOM_MANAGEMENT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) BIND_AUTO_CREATE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "BIND_AUTO_CREATE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) BIND_DEBUG_UNBIND() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "BIND_DEBUG_UNBIND", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) BIND_EXTERNAL_SERVICE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "BIND_EXTERNAL_SERVICE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) BIND_IMPORTANT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "BIND_IMPORTANT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) BIND_INCLUDE_CAPABILITIES() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "BIND_INCLUDE_CAPABILITIES", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) BIND_NOT_FOREGROUND() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "BIND_NOT_FOREGROUND", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) BIND_NOT_PERCEPTIBLE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "BIND_NOT_PERCEPTIBLE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) BIND_WAIVE_PRIORITY() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "BIND_WAIVE_PRIORITY", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) BIOMETRIC_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "BIOMETRIC_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) BLOB_STORE_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "BLOB_STORE_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) BLUETOOTH_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "BLUETOOTH_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) BUGREPORT_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "BUGREPORT_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) CAMERA_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CAMERA_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) CAPTIONING_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CAPTIONING_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) CARRIER_CONFIG_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CARRIER_CONFIG_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) CLIPBOARD_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CLIPBOARD_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) COMPANION_DEVICE_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "COMPANION_DEVICE_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) CONNECTIVITY_DIAGNOSTICS_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONNECTIVITY_DIAGNOSTICS_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) CONNECTIVITY_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONNECTIVITY_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) CONSUMER_IR_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONSUMER_IR_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) CONTEXT_IGNORE_SECURITY() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTEXT_IGNORE_SECURITY", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) CONTEXT_INCLUDE_CODE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTEXT_INCLUDE_CODE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) CONTEXT_RESTRICTED() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTEXT_RESTRICTED", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) CROSS_PROFILE_APPS_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CROSS_PROFILE_APPS_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) DEVICE_POLICY_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DEVICE_POLICY_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) DISPLAY_HASH_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DISPLAY_HASH_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) DISPLAY_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DISPLAY_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) DOMAIN_VERIFICATION_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DOMAIN_VERIFICATION_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) DOWNLOAD_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DOWNLOAD_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) DROPBOX_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DROPBOX_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) EUICC_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EUICC_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) FILE_INTEGRITY_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FILE_INTEGRITY_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) FINGERPRINT_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FINGERPRINT_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) GAME_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "GAME_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) HARDWARE_PROPERTIES_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "HARDWARE_PROPERTIES_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) INPUT_METHOD_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "INPUT_METHOD_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) INPUT_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "INPUT_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) IPSEC_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "IPSEC_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) JOB_SCHEDULER_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "JOB_SCHEDULER_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) KEYGUARD_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "KEYGUARD_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) LAUNCHER_APPS_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LAUNCHER_APPS_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) LAYOUT_INFLATER_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LAYOUT_INFLATER_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) LOCATION_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LOCATION_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) MEDIA_COMMUNICATION_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MEDIA_COMMUNICATION_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) MEDIA_METRICS_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MEDIA_METRICS_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) MEDIA_PROJECTION_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MEDIA_PROJECTION_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) MEDIA_ROUTER_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MEDIA_ROUTER_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) MEDIA_SESSION_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MEDIA_SESSION_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) MIDI_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MIDI_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) MODE_APPEND() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MODE_APPEND", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) MODE_ENABLE_WRITE_AHEAD_LOGGING() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MODE_ENABLE_WRITE_AHEAD_LOGGING", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) MODE_MULTI_PROCESS() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MODE_MULTI_PROCESS", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) MODE_NO_LOCALIZED_COLLATORS() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MODE_NO_LOCALIZED_COLLATORS", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) MODE_PRIVATE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MODE_PRIVATE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) MODE_WORLD_READABLE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MODE_WORLD_READABLE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) MODE_WORLD_WRITEABLE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MODE_WORLD_WRITEABLE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) NETWORK_STATS_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "NETWORK_STATS_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) NFC_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "NFC_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) NOTIFICATION_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "NOTIFICATION_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) NSD_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "NSD_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) PEOPLE_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "PEOPLE_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) PERFORMANCE_HINT_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "PERFORMANCE_HINT_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) POWER_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "POWER_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) PRINT_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "PRINT_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) RECEIVER_VISIBLE_TO_INSTANT_APPS() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "RECEIVER_VISIBLE_TO_INSTANT_APPS", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) RESTRICTIONS_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "RESTRICTIONS_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) ROLE_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ROLE_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) SEARCH_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SEARCH_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) SENSOR_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) SHORTCUT_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SHORTCUT_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) STORAGE_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "STORAGE_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) STORAGE_STATS_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "STORAGE_STATS_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) SYSTEM_HEALTH_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SYSTEM_HEALTH_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) TELECOM_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "TELECOM_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) TELEPHONY_IMS_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "TELEPHONY_IMS_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) TELEPHONY_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "TELEPHONY_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) TELEPHONY_SUBSCRIPTION_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "TELEPHONY_SUBSCRIPTION_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) TEXT_CLASSIFICATION_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "TEXT_CLASSIFICATION_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) TEXT_SERVICES_MANAGER_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "TEXT_SERVICES_MANAGER_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) TV_INPUT_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "TV_INPUT_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) UI_MODE_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "UI_MODE_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) USAGE_STATS_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "USAGE_STATS_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) USB_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "USB_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) USER_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "USER_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) VIBRATOR_MANAGER_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "VIBRATOR_MANAGER_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) VIBRATOR_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "VIBRATOR_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) VPN_MANAGEMENT_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "VPN_MANAGEMENT_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) WALLPAPER_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "WALLPAPER_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) WIFI_AWARE_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "WIFI_AWARE_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) WIFI_P2P_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "WIFI_P2P_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) WIFI_RTT_RANGING_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "WIFI_RTT_RANGING_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) WIFI_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "WIFI_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) WINDOW_SERVICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "WINDOW_SERVICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ContextClass) Android_content_Context() (*Android_content_ContextInstance, error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "()V")
	obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id)
	if err != nil {
		return nil, err
	}
	return Android_content_Context_Instance(e.vmc, obj), nil
}

func (e *Android_content_ContextInstance) BindIsolatedService(arg0 jni.Object, arg1 int32, arg2 jni.Object, arg3 jni.Object, arg4 jni.Object) (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "bindIsolatedService", "(Landroid/content/Intent;ILjava/lang/String;Ljava/util/concurrent/Executor;Landroid/content/ServiceConnection;)Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)), jni.Value(ObjVal(arg2)), jni.Value(ObjVal(arg3)), jni.Value(ObjVal(arg4)))
}

func (e *Android_content_ContextInstance) BindService(args ...interface{}) (ret bool, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 3 && strings.HasPrefix(types[0], "android/content/Intent") && strings.HasPrefix(types[1], "android/content/ServiceConnection") && types[2] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "bindService", "(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z")
		return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))), jni.Value((args[2].(int32))))
	case len(args) == 4 && strings.HasPrefix(types[0], "android/content/Intent") && types[1] == "int32" && strings.HasPrefix(types[2], "java/util/concurrent/Executor") && strings.HasPrefix(types[3], "android/content/ServiceConnection"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "bindService", "(Landroid/content/Intent;ILjava/util/concurrent/Executor;Landroid/content/ServiceConnection;)Z")
		return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))), jni.Value(ObjVal(args[2].(jni.Object))), jni.Value(ObjVal(args[3].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_ContextInstance) BindServiceAsUser(arg0 jni.Object, arg1 jni.Object, arg2 int32, arg3 jni.Object) (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "bindServiceAsUser", "(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)), jni.Value((arg2)), jni.Value(ObjVal(arg3)))
}

func (e *Android_content_ContextInstance) CheckCallingOrSelfPermission(arg0 jni.Object) (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "checkCallingOrSelfPermission", "(Ljava/lang/String;)I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) CheckCallingOrSelfUriPermission(arg0 jni.Object, arg1 int32) (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "checkCallingOrSelfUriPermission", "(Landroid/net/Uri;I)I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)))
}

func (e *Android_content_ContextInstance) CheckCallingOrSelfUriPermissions(arg0 jni.Object, arg1 int32) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "checkCallingOrSelfUriPermissions", "(Ljava/util/List;I)[I")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)))
}

func (e *Android_content_ContextInstance) CheckCallingPermission(arg0 jni.Object) (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "checkCallingPermission", "(Ljava/lang/String;)I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) CheckCallingUriPermission(arg0 jni.Object, arg1 int32) (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "checkCallingUriPermission", "(Landroid/net/Uri;I)I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)))
}

func (e *Android_content_ContextInstance) CheckCallingUriPermissions(arg0 jni.Object, arg1 int32) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "checkCallingUriPermissions", "(Ljava/util/List;I)[I")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)))
}

func (e *Android_content_ContextInstance) CheckPermission(arg0 jni.Object, arg1 int32, arg2 int32) (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "checkPermission", "(Ljava/lang/String;II)I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)), jni.Value((arg2)))
}

func (e *Android_content_ContextInstance) CheckSelfPermission(arg0 jni.Object) (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "checkSelfPermission", "(Ljava/lang/String;)I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) CheckUriPermission(args ...interface{}) (ret int32, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 4 && strings.HasPrefix(types[0], "android/net/Uri") && types[1] == "int32" && types[2] == "int32" && types[3] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "checkUriPermission", "(Landroid/net/Uri;III)I")
		return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))), jni.Value((args[2].(int32))), jni.Value((args[3].(int32))))
	case len(args) == 6 && strings.HasPrefix(types[0], "android/net/Uri") && strings.HasPrefix(types[1], "java/lang/String") && strings.HasPrefix(types[2], "java/lang/String") && types[3] == "int32" && types[4] == "int32" && types[5] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "checkUriPermission", "(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;III)I")
		return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))), jni.Value(ObjVal(args[2].(jni.Object))), jni.Value((args[3].(int32))), jni.Value((args[4].(int32))), jni.Value((args[5].(int32))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_ContextInstance) CheckUriPermissions(arg0 jni.Object, arg1 int32, arg2 int32, arg3 int32) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "checkUriPermissions", "(Ljava/util/List;III)[I")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)), jni.Value((arg2)), jni.Value((arg3)))
}

func (e *Android_content_ContextInstance) ClearWallpaper() (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "clearWallpaper", "()V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) CreateAttributionContext(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "createAttributionContext", "(Ljava/lang/String;)Landroid/content/Context;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) CreateConfigurationContext(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "createConfigurationContext", "(Landroid/content/res/Configuration;)Landroid/content/Context;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) CreateContext(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "createContext", "(Landroid/content/ContextParams;)Landroid/content/Context;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) CreateContextForSplit(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "createContextForSplit", "(Ljava/lang/String;)Landroid/content/Context;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) CreateDeviceProtectedStorageContext() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "createDeviceProtectedStorageContext", "()Landroid/content/Context;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) CreateDisplayContext(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "createDisplayContext", "(Landroid/view/Display;)Landroid/content/Context;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) CreatePackageContext(arg0 jni.Object, arg1 int32) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "createPackageContext", "(Ljava/lang/String;I)Landroid/content/Context;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)))
}

func (e *Android_content_ContextInstance) CreateWindowContext(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 2 && types[0] == "int32" && strings.HasPrefix(types[1], "android/os/Bundle"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "createWindowContext", "(ILandroid/os/Bundle;)Landroid/content/Context;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((args[0].(int32))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 3 && strings.HasPrefix(types[0], "android/view/Display") && types[1] == "int32" && strings.HasPrefix(types[2], "android/os/Bundle"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "createWindowContext", "(Landroid/view/Display;ILandroid/os/Bundle;)Landroid/content/Context;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))), jni.Value(ObjVal(args[2].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_ContextInstance) DatabaseList() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "databaseList", "()[Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) DeleteDatabase(arg0 jni.Object) (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "deleteDatabase", "(Ljava/lang/String;)Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) DeleteFile(arg0 jni.Object) (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "deleteFile", "(Ljava/lang/String;)Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) DeleteSharedPreferences(arg0 jni.Object) (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "deleteSharedPreferences", "(Ljava/lang/String;)Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) EnforceCallingOrSelfPermission(arg0 jni.Object, arg1 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "enforceCallingOrSelfPermission", "(Ljava/lang/String;Ljava/lang/String;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)))
}

func (e *Android_content_ContextInstance) EnforceCallingOrSelfUriPermission(arg0 jni.Object, arg1 int32, arg2 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "enforceCallingOrSelfUriPermission", "(Landroid/net/Uri;ILjava/lang/String;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)), jni.Value(ObjVal(arg2)))
}

func (e *Android_content_ContextInstance) EnforceCallingPermission(arg0 jni.Object, arg1 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "enforceCallingPermission", "(Ljava/lang/String;Ljava/lang/String;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)))
}

func (e *Android_content_ContextInstance) EnforceCallingUriPermission(arg0 jni.Object, arg1 int32, arg2 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "enforceCallingUriPermission", "(Landroid/net/Uri;ILjava/lang/String;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)), jni.Value(ObjVal(arg2)))
}

func (e *Android_content_ContextInstance) EnforcePermission(arg0 jni.Object, arg1 int32, arg2 int32, arg3 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "enforcePermission", "(Ljava/lang/String;IILjava/lang/String;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)), jni.Value((arg2)), jni.Value(ObjVal(arg3)))
}

func (e *Android_content_ContextInstance) EnforceUriPermission(args ...interface{}) (err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 5 && strings.HasPrefix(types[0], "android/net/Uri") && types[1] == "int32" && types[2] == "int32" && types[3] == "int32" && strings.HasPrefix(types[4], "java/lang/String"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "enforceUriPermission", "(Landroid/net/Uri;IIILjava/lang/String;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))), jni.Value((args[2].(int32))), jni.Value((args[3].(int32))), jni.Value(ObjVal(args[4].(jni.Object))))
	case len(args) == 7 && strings.HasPrefix(types[0], "android/net/Uri") && strings.HasPrefix(types[1], "java/lang/String") && strings.HasPrefix(types[2], "java/lang/String") && types[3] == "int32" && types[4] == "int32" && types[5] == "int32" && strings.HasPrefix(types[6], "java/lang/String"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "enforceUriPermission", "(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))), jni.Value(ObjVal(args[2].(jni.Object))), jni.Value((args[3].(int32))), jni.Value((args[4].(int32))), jni.Value((args[5].(int32))), jni.Value(ObjVal(args[6].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_ContextInstance) FileList() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "fileList", "()[Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetApplicationContext() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getApplicationContext", "()Landroid/content/Context;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetApplicationInfo() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getApplicationInfo", "()Landroid/content/pm/ApplicationInfo;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetAssets() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getAssets", "()Landroid/content/res/AssetManager;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetAttributionSource() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getAttributionSource", "()Landroid/content/AttributionSource;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetAttributionTag() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getAttributionTag", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetCacheDir() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getCacheDir", "()Ljava/io/File;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetClassLoader() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getClassLoader", "()Ljava/lang/ClassLoader;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetCodeCacheDir() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getCodeCacheDir", "()Ljava/io/File;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetColor(arg0 int32) (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getColor", "(I)I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((arg0)))
}

func (e *Android_content_ContextInstance) GetColorStateList(arg0 int32) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getColorStateList", "(I)Landroid/content/res/ColorStateList;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((arg0)))
}

func (e *Android_content_ContextInstance) GetContentResolver() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getContentResolver", "()Landroid/content/ContentResolver;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetDataDir() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getDataDir", "()Ljava/io/File;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetDatabasePath(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getDatabasePath", "(Ljava/lang/String;)Ljava/io/File;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) GetDir(arg0 jni.Object, arg1 int32) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getDir", "(Ljava/lang/String;I)Ljava/io/File;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)))
}

func (e *Android_content_ContextInstance) GetDisplay() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getDisplay", "()Landroid/view/Display;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetDrawable(arg0 int32) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getDrawable", "(I)Landroid/graphics/drawable/Drawable;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((arg0)))
}

func (e *Android_content_ContextInstance) GetExternalCacheDir() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getExternalCacheDir", "()Ljava/io/File;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetExternalCacheDirs() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getExternalCacheDirs", "()[Ljava/io/File;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetExternalFilesDir(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getExternalFilesDir", "(Ljava/lang/String;)Ljava/io/File;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) GetExternalFilesDirs(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getExternalFilesDirs", "(Ljava/lang/String;)[Ljava/io/File;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) GetExternalMediaDirs() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getExternalMediaDirs", "()[Ljava/io/File;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetFileStreamPath(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getFileStreamPath", "(Ljava/lang/String;)Ljava/io/File;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) GetFilesDir() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getFilesDir", "()Ljava/io/File;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetMainExecutor() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getMainExecutor", "()Ljava/util/concurrent/Executor;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetMainLooper() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getMainLooper", "()Landroid/os/Looper;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetNoBackupFilesDir() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getNoBackupFilesDir", "()Ljava/io/File;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetObbDir() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getObbDir", "()Ljava/io/File;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetObbDirs() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getObbDirs", "()[Ljava/io/File;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetOpPackageName() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getOpPackageName", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetPackageCodePath() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getPackageCodePath", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetPackageManager() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getPackageManager", "()Landroid/content/pm/PackageManager;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetPackageName() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getPackageName", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetPackageResourcePath() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getPackageResourcePath", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetParams() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getParams", "()Landroid/content/ContextParams;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetResources() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getResources", "()Landroid/content/res/Resources;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetSharedPreferences(arg0 jni.Object, arg1 int32) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getSharedPreferences", "(Ljava/lang/String;I)Landroid/content/SharedPreferences;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)))
}

func (e *Android_content_ContextInstance) GetString(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 1 && types[0] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getString", "(I)Ljava/lang/String;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((args[0].(int32))))
	case len(args) == 2 && types[0] == "int32" && strings.HasPrefix(types[1], "[Ljava/lang/Object;"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getString", "(I[Ljava/lang/Object;)Ljava/lang/String;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((args[0].(int32))), jni.Value(ObjVal(args[1].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_ContextInstance) GetSystemService(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 1 && strings.HasPrefix(types[0], "java/lang/String"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getSystemService", "(Ljava/lang/String;)Ljava/lang/Object;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 1 && strings.HasPrefix(types[0], "java/lang/Class"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getSystemService", "(Ljava/lang/Class;)Ljava/lang/Object;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_ContextInstance) GetSystemServiceName(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getSystemServiceName", "(Ljava/lang/Class;)Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) GetText(arg0 int32) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getText", "(I)Ljava/lang/CharSequence;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((arg0)))
}

func (e *Android_content_ContextInstance) GetTheme() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getTheme", "()Landroid/content/res/Resources$Theme;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetWallpaper() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getWallpaper", "()Landroid/graphics/drawable/Drawable;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetWallpaperDesiredMinimumHeight() (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getWallpaperDesiredMinimumHeight", "()I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GetWallpaperDesiredMinimumWidth() (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getWallpaperDesiredMinimumWidth", "()I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) GrantUriPermission(arg0 jni.Object, arg1 jni.Object, arg2 int32) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "grantUriPermission", "(Ljava/lang/String;Landroid/net/Uri;I)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)), jni.Value((arg2)))
}

func (e *Android_content_ContextInstance) IsDeviceProtectedStorage() (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "isDeviceProtectedStorage", "()Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) IsRestricted() (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "isRestricted", "()Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) IsUiContext() (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "isUiContext", "()Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) MoveDatabaseFrom(arg0 jni.Object, arg1 jni.Object) (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "moveDatabaseFrom", "(Landroid/content/Context;Ljava/lang/String;)Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)))
}

func (e *Android_content_ContextInstance) MoveSharedPreferencesFrom(arg0 jni.Object, arg1 jni.Object) (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "moveSharedPreferencesFrom", "(Landroid/content/Context;Ljava/lang/String;)Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)))
}

func (e *Android_content_ContextInstance) ObtainStyledAttributes(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 1 && strings.HasPrefix(types[0], "[I"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "obtainStyledAttributes", "([I)Landroid/content/res/TypedArray;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 2 && types[0] == "int32" && strings.HasPrefix(types[1], "[I"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "obtainStyledAttributes", "(I[I)Landroid/content/res/TypedArray;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((args[0].(int32))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "android/util/AttributeSet") && strings.HasPrefix(types[1], "[I"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "obtainStyledAttributes", "(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 4 && strings.HasPrefix(types[0], "android/util/AttributeSet") && strings.HasPrefix(types[1], "[I") && types[2] == "int32" && types[3] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "obtainStyledAttributes", "(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))), jni.Value((args[2].(int32))), jni.Value((args[3].(int32))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_ContextInstance) OpenFileInput(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "openFileInput", "(Ljava/lang/String;)Ljava/io/FileInputStream;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) OpenFileOutput(arg0 jni.Object, arg1 int32) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "openFileOutput", "(Ljava/lang/String;I)Ljava/io/FileOutputStream;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)))
}

func (e *Android_content_ContextInstance) OpenOrCreateDatabase(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 3 && strings.HasPrefix(types[0], "java/lang/String") && types[1] == "int32" && strings.HasPrefix(types[2], "android/database/sqlite/SQLiteDatabase$CursorFactory"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "openOrCreateDatabase", "(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))), jni.Value(ObjVal(args[2].(jni.Object))))
	case len(args) == 4 && strings.HasPrefix(types[0], "java/lang/String") && types[1] == "int32" && strings.HasPrefix(types[2], "android/database/sqlite/SQLiteDatabase$CursorFactory") && strings.HasPrefix(types[3], "android/database/DatabaseErrorHandler"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "openOrCreateDatabase", "(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;Landroid/database/DatabaseErrorHandler;)Landroid/database/sqlite/SQLiteDatabase;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))), jni.Value(ObjVal(args[2].(jni.Object))), jni.Value(ObjVal(args[3].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_ContextInstance) PeekWallpaper() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "peekWallpaper", "()Landroid/graphics/drawable/Drawable;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ContextInstance) RegisterComponentCallbacks(arg0 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "registerComponentCallbacks", "(Landroid/content/ComponentCallbacks;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) RegisterReceiver(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 2 && strings.HasPrefix(types[0], "android/content/BroadcastReceiver") && strings.HasPrefix(types[1], "android/content/IntentFilter"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "registerReceiver", "(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 3 && strings.HasPrefix(types[0], "android/content/BroadcastReceiver") && strings.HasPrefix(types[1], "android/content/IntentFilter") && types[2] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "registerReceiver", "(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))), jni.Value((args[2].(int32))))
	case len(args) == 4 && strings.HasPrefix(types[0], "android/content/BroadcastReceiver") && strings.HasPrefix(types[1], "android/content/IntentFilter") && strings.HasPrefix(types[2], "java/lang/String") && strings.HasPrefix(types[3], "android/os/Handler"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "registerReceiver", "(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))), jni.Value(ObjVal(args[2].(jni.Object))), jni.Value(ObjVal(args[3].(jni.Object))))
	case len(args) == 5 && strings.HasPrefix(types[0], "android/content/BroadcastReceiver") && strings.HasPrefix(types[1], "android/content/IntentFilter") && strings.HasPrefix(types[2], "java/lang/String") && strings.HasPrefix(types[3], "android/os/Handler") && types[4] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "registerReceiver", "(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;I)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))), jni.Value(ObjVal(args[2].(jni.Object))), jni.Value(ObjVal(args[3].(jni.Object))), jni.Value((args[4].(int32))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_ContextInstance) RemoveStickyBroadcast(arg0 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "removeStickyBroadcast", "(Landroid/content/Intent;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) RemoveStickyBroadcastAsUser(arg0 jni.Object, arg1 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "removeStickyBroadcastAsUser", "(Landroid/content/Intent;Landroid/os/UserHandle;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)))
}

func (e *Android_content_ContextInstance) RevokeUriPermission(args ...interface{}) (err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 2 && strings.HasPrefix(types[0], "android/net/Uri") && types[1] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "revokeUriPermission", "(Landroid/net/Uri;I)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))))
	case len(args) == 3 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "android/net/Uri") && types[2] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "revokeUriPermission", "(Ljava/lang/String;Landroid/net/Uri;I)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))), jni.Value((args[2].(int32))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_ContextInstance) SendBroadcast(args ...interface{}) (err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 1 && strings.HasPrefix(types[0], "android/content/Intent"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "sendBroadcast", "(Landroid/content/Intent;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "android/content/Intent") && strings.HasPrefix(types[1], "java/lang/String"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "sendBroadcast", "(Landroid/content/Intent;Ljava/lang/String;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_ContextInstance) SendBroadcastAsUser(args ...interface{}) (err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 2 && strings.HasPrefix(types[0], "android/content/Intent") && strings.HasPrefix(types[1], "android/os/UserHandle"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "sendBroadcastAsUser", "(Landroid/content/Intent;Landroid/os/UserHandle;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 3 && strings.HasPrefix(types[0], "android/content/Intent") && strings.HasPrefix(types[1], "android/os/UserHandle") && strings.HasPrefix(types[2], "java/lang/String"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "sendBroadcastAsUser", "(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))), jni.Value(ObjVal(args[2].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_ContextInstance) SendBroadcastWithMultiplePermissions(arg0 jni.Object, arg1 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "sendBroadcastWithMultiplePermissions", "(Landroid/content/Intent;[Ljava/lang/String;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)))
}

func (e *Android_content_ContextInstance) SendOrderedBroadcast(args ...interface{}) (err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 2 && strings.HasPrefix(types[0], "android/content/Intent") && strings.HasPrefix(types[1], "java/lang/String"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "sendOrderedBroadcast", "(Landroid/content/Intent;Ljava/lang/String;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 7 && strings.HasPrefix(types[0], "android/content/Intent") && strings.HasPrefix(types[1], "java/lang/String") && strings.HasPrefix(types[2], "android/content/BroadcastReceiver") && strings.HasPrefix(types[3], "android/os/Handler") && types[4] == "int32" && strings.HasPrefix(types[5], "java/lang/String") && strings.HasPrefix(types[6], "android/os/Bundle"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "sendOrderedBroadcast", "(Landroid/content/Intent;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))), jni.Value(ObjVal(args[2].(jni.Object))), jni.Value(ObjVal(args[3].(jni.Object))), jni.Value((args[4].(int32))), jni.Value(ObjVal(args[5].(jni.Object))), jni.Value(ObjVal(args[6].(jni.Object))))
	case len(args) == 8 && strings.HasPrefix(types[0], "android/content/Intent") && strings.HasPrefix(types[1], "java/lang/String") && strings.HasPrefix(types[2], "java/lang/String") && strings.HasPrefix(types[3], "android/content/BroadcastReceiver") && strings.HasPrefix(types[4], "android/os/Handler") && types[5] == "int32" && strings.HasPrefix(types[6], "java/lang/String") && strings.HasPrefix(types[7], "android/os/Bundle"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "sendOrderedBroadcast", "(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))), jni.Value(ObjVal(args[2].(jni.Object))), jni.Value(ObjVal(args[3].(jni.Object))), jni.Value(ObjVal(args[4].(jni.Object))), jni.Value((args[5].(int32))), jni.Value(ObjVal(args[6].(jni.Object))), jni.Value(ObjVal(args[7].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_ContextInstance) SendOrderedBroadcastAsUser(arg0 jni.Object, arg1 jni.Object, arg2 jni.Object, arg3 jni.Object, arg4 jni.Object, arg5 int32, arg6 jni.Object, arg7 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "sendOrderedBroadcastAsUser", "(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)), jni.Value(ObjVal(arg2)), jni.Value(ObjVal(arg3)), jni.Value(ObjVal(arg4)), jni.Value((arg5)), jni.Value(ObjVal(arg6)), jni.Value(ObjVal(arg7)))
}

func (e *Android_content_ContextInstance) SendStickyBroadcast(args ...interface{}) (err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 1 && strings.HasPrefix(types[0], "android/content/Intent"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "sendStickyBroadcast", "(Landroid/content/Intent;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "android/content/Intent") && strings.HasPrefix(types[1], "android/os/Bundle"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "sendStickyBroadcast", "(Landroid/content/Intent;Landroid/os/Bundle;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_ContextInstance) SendStickyBroadcastAsUser(arg0 jni.Object, arg1 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "sendStickyBroadcastAsUser", "(Landroid/content/Intent;Landroid/os/UserHandle;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)))
}

func (e *Android_content_ContextInstance) SendStickyOrderedBroadcast(arg0 jni.Object, arg1 jni.Object, arg2 jni.Object, arg3 int32, arg4 jni.Object, arg5 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "sendStickyOrderedBroadcast", "(Landroid/content/Intent;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)), jni.Value(ObjVal(arg2)), jni.Value((arg3)), jni.Value(ObjVal(arg4)), jni.Value(ObjVal(arg5)))
}

func (e *Android_content_ContextInstance) SendStickyOrderedBroadcastAsUser(arg0 jni.Object, arg1 jni.Object, arg2 jni.Object, arg3 jni.Object, arg4 int32, arg5 jni.Object, arg6 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "sendStickyOrderedBroadcastAsUser", "(Landroid/content/Intent;Landroid/os/UserHandle;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)), jni.Value(ObjVal(arg2)), jni.Value(ObjVal(arg3)), jni.Value((arg4)), jni.Value(ObjVal(arg5)), jni.Value(ObjVal(arg6)))
}

func (e *Android_content_ContextInstance) SetTheme(arg0 int32) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setTheme", "(I)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((arg0)))
}

func (e *Android_content_ContextInstance) SetWallpaper(args ...interface{}) (err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 1 && strings.HasPrefix(types[0], "android/graphics/Bitmap"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setWallpaper", "(Landroid/graphics/Bitmap;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 1 && strings.HasPrefix(types[0], "java/io/InputStream"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setWallpaper", "(Ljava/io/InputStream;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_ContextInstance) StartActivities(args ...interface{}) (err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 1 && strings.HasPrefix(types[0], "[Landroid/content/Intent;"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "startActivities", "([Landroid/content/Intent;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "[Landroid/content/Intent;") && strings.HasPrefix(types[1], "android/os/Bundle"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "startActivities", "([Landroid/content/Intent;Landroid/os/Bundle;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_ContextInstance) StartActivity(args ...interface{}) (err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 1 && strings.HasPrefix(types[0], "android/content/Intent"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "startActivity", "(Landroid/content/Intent;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "android/content/Intent") && strings.HasPrefix(types[1], "android/os/Bundle"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "startActivity", "(Landroid/content/Intent;Landroid/os/Bundle;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_ContextInstance) StartForegroundService(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "startForegroundService", "(Landroid/content/Intent;)Landroid/content/ComponentName;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) StartInstrumentation(arg0 jni.Object, arg1 jni.Object, arg2 jni.Object) (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "startInstrumentation", "(Landroid/content/ComponentName;Ljava/lang/String;Landroid/os/Bundle;)Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)), jni.Value(ObjVal(arg2)))
}

func (e *Android_content_ContextInstance) StartIntentSender(args ...interface{}) (err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 5 && strings.HasPrefix(types[0], "android/content/IntentSender") && strings.HasPrefix(types[1], "android/content/Intent") && types[2] == "int32" && types[3] == "int32" && types[4] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "startIntentSender", "(Landroid/content/IntentSender;Landroid/content/Intent;III)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))), jni.Value((args[2].(int32))), jni.Value((args[3].(int32))), jni.Value((args[4].(int32))))
	case len(args) == 6 && strings.HasPrefix(types[0], "android/content/IntentSender") && strings.HasPrefix(types[1], "android/content/Intent") && types[2] == "int32" && types[3] == "int32" && types[4] == "int32" && strings.HasPrefix(types[5], "android/os/Bundle"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "startIntentSender", "(Landroid/content/IntentSender;Landroid/content/Intent;IIILandroid/os/Bundle;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))), jni.Value((args[2].(int32))), jni.Value((args[3].(int32))), jni.Value((args[4].(int32))), jni.Value(ObjVal(args[5].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_ContextInstance) StartService(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "startService", "(Landroid/content/Intent;)Landroid/content/ComponentName;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) StopService(arg0 jni.Object) (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "stopService", "(Landroid/content/Intent;)Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) UnbindService(arg0 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "unbindService", "(Landroid/content/ServiceConnection;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) UnregisterComponentCallbacks(arg0 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "unregisterComponentCallbacks", "(Landroid/content/ComponentCallbacks;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) UnregisterReceiver(arg0 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "unregisterReceiver", "(Landroid/content/BroadcastReceiver;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ContextInstance) UpdateServiceGroup(arg0 jni.Object, arg1 int32, arg2 int32) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "updateServiceGroup", "(Landroid/content/ServiceConnection;II)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)), jni.Value((arg2)))
}

