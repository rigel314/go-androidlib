package androidlib

import (
	"math"
	"strings"

	"git.wow.st/gmp/jni"
)

var _ = math.Float32bits
var _ = strings.HasPrefix

type Java_lang_ObjectClass struct {
	vmc   *VMContext
	Class jni.Class
}

type Java_lang_ObjectInstance struct {
	*Java_lang_ObjectClass
	Jobj jni.Object
}

func Java_lang_Object(vmc *VMContext) *Java_lang_ObjectClass {
	return &Java_lang_ObjectClass{
		vmc:   vmc,
		Class: jni.FindClass(vmc.JNIEnv, "java/lang/Object"),
	}
}

func Java_lang_Object_Instance(vmc *VMContext, jobj jni.Object) *Java_lang_ObjectInstance {
	return &Java_lang_ObjectInstance{
		Java_lang_ObjectClass: Java_lang_Object(vmc),
		Jobj: jobj,
	}
}

func (e *Java_lang_ObjectInstance) Equals(arg0 jni.Object) (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "equals", "(Ljava/lang/Object;)Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Java_lang_ObjectInstance) GetClass() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getClass", "()Ljava/lang/Class;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Java_lang_ObjectInstance) HashCode() (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "hashCode", "()I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Java_lang_ObjectClass) Java_lang_Object() (*Java_lang_ObjectInstance, error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "()V")
	obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id)
	if err != nil {
		return nil, err
	}
	return Java_lang_Object_Instance(e.vmc, obj), nil
}

func (e *Java_lang_ObjectInstance) Notify() (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "notify", "()V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Java_lang_ObjectInstance) NotifyAll() (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "notifyAll", "()V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Java_lang_ObjectInstance) ToString() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "toString", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Java_lang_ObjectInstance) Wait(args ...interface{}) (err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 1 && types[0] == "int64":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "wait", "(J)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((args[0].(int64))))
	case len(args) == 2 && types[0] == "int64" && types[1] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "wait", "(JI)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((args[0].(int64))), jni.Value((args[1].(int32))))
	case len(args) == 0:
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "wait", "()V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id)
	default:
		err = overloadError(types)
		return
	}
}

