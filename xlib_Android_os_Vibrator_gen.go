package androidlib

import (
	"math"
	"strings"

	"git.wow.st/gmp/jni"
)

var _ = math.Float32bits
var _ = strings.HasPrefix

type Android_os_VibratorClass struct {
	vmc   *VMContext
	Class jni.Class
}

type Android_os_VibratorInstance struct {
	*Android_os_VibratorClass
	Jobj jni.Object
}

func Android_os_Vibrator(vmc *VMContext) *Android_os_VibratorClass {
	return &Android_os_VibratorClass{
		vmc:   vmc,
		Class: jni.FindClass(vmc.JNIEnv, "android/os/Vibrator"),
	}
}

func Android_os_Vibrator_Instance(vmc *VMContext, jobj jni.Object) *Android_os_VibratorInstance {
	return &Android_os_VibratorInstance{
		Android_os_VibratorClass: Android_os_Vibrator(vmc),
		Jobj: jobj,
	}
}

func (e *Android_os_VibratorClass) VIBRATION_EFFECT_SUPPORT_NO() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "VIBRATION_EFFECT_SUPPORT_NO", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_VibratorClass) VIBRATION_EFFECT_SUPPORT_UNKNOWN() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "VIBRATION_EFFECT_SUPPORT_UNKNOWN", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_VibratorClass) VIBRATION_EFFECT_SUPPORT_YES() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "VIBRATION_EFFECT_SUPPORT_YES", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_VibratorInstance) AreAllEffectsSupported(arg0 jni.Object) (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "areAllEffectsSupported", "([I)I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_os_VibratorInstance) AreAllPrimitivesSupported(arg0 jni.Object) (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "areAllPrimitivesSupported", "([I)Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_os_VibratorInstance) AreEffectsSupported(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "areEffectsSupported", "([I)[I")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_os_VibratorInstance) ArePrimitivesSupported(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "arePrimitivesSupported", "([I)[Z")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_os_VibratorInstance) Cancel() (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "cancel", "()V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_os_VibratorInstance) GetId() (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getId", "()I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_os_VibratorInstance) GetPrimitiveDurations(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getPrimitiveDurations", "([I)[I")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_os_VibratorInstance) HasAmplitudeControl() (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "hasAmplitudeControl", "()Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_os_VibratorInstance) HasVibrator() (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "hasVibrator", "()Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_os_VibratorInstance) Vibrate(args ...interface{}) (err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 1 && types[0] == "int64":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "vibrate", "(J)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((args[0].(int64))))
	case len(args) == 2 && types[0] == "int64" && strings.HasPrefix(types[1], "android/media/AudioAttributes"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "vibrate", "(JLandroid/media/AudioAttributes;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((args[0].(int64))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "[J") && types[1] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "vibrate", "([JI)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))))
	case len(args) == 3 && strings.HasPrefix(types[0], "[J") && types[1] == "int32" && strings.HasPrefix(types[2], "android/media/AudioAttributes"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "vibrate", "([JILandroid/media/AudioAttributes;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))), jni.Value(ObjVal(args[2].(jni.Object))))
	case len(args) == 1 && strings.HasPrefix(types[0], "android/os/VibrationEffect"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "vibrate", "(Landroid/os/VibrationEffect;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "android/os/VibrationEffect") && strings.HasPrefix(types[1], "android/media/AudioAttributes"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "vibrate", "(Landroid/os/VibrationEffect;Landroid/media/AudioAttributes;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

