package androidlib

import (
	"math"
	"strings"

	"git.wow.st/gmp/jni"
)

var _ = math.Float32bits
var _ = strings.HasPrefix

type Java_lang_IntegerClass struct {
	vmc   *VMContext
	Class jni.Class
}

type Java_lang_IntegerInstance struct {
	*Java_lang_IntegerClass
	Jobj jni.Object
}

func Java_lang_Integer(vmc *VMContext) *Java_lang_IntegerClass {
	return &Java_lang_IntegerClass{
		vmc:   vmc,
		Class: jni.FindClass(vmc.JNIEnv, "java/lang/Integer"),
	}
}

func Java_lang_Integer_Instance(vmc *VMContext, jobj jni.Object) *Java_lang_IntegerInstance {
	return &Java_lang_IntegerInstance{
		Java_lang_IntegerClass: Java_lang_Integer(vmc),
		Jobj: jobj,
	}
}

func (e *Java_lang_IntegerClass) BYTES() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "BYTES", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Java_lang_IntegerClass) MAX_VALUE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MAX_VALUE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Java_lang_IntegerClass) MIN_VALUE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MIN_VALUE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Java_lang_IntegerClass) SIZE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SIZE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Java_lang_IntegerClass) TYPE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "TYPE", "Ljava/lang/Class;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Java_lang_IntegerClass) BitCount(arg0 int32) (ret int32, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "bitCount", "(I)I")
	return jni.CallStaticIntMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((arg0)))
}

func (e *Java_lang_IntegerInstance) ByteValue() (ret byte, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "byteValue", "()B")
	return jni.CallByteMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Java_lang_IntegerClass) Compare(arg0 int32, arg1 int32) (ret int32, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "compare", "(II)I")
	return jni.CallStaticIntMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((arg0)), jni.Value((arg1)))
}

func (e *Java_lang_IntegerInstance) CompareTo(args ...interface{}) (ret int32, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 1 && strings.HasPrefix(types[0], "java/lang/Integer"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "compareTo", "(Ljava/lang/Integer;)I")
		return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 1 && strings.HasPrefix(types[0], "java/lang/Object"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "compareTo", "(Ljava/lang/Object;)I")
		return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Java_lang_IntegerClass) CompareUnsigned(arg0 int32, arg1 int32) (ret int32, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "compareUnsigned", "(II)I")
	return jni.CallStaticIntMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((arg0)), jni.Value((arg1)))
}

func (e *Java_lang_IntegerClass) Decode(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "decode", "(Ljava/lang/String;)Ljava/lang/Integer;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(arg0)))
}

func (e *Java_lang_IntegerClass) DivideUnsigned(arg0 int32, arg1 int32) (ret int32, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "divideUnsigned", "(II)I")
	return jni.CallStaticIntMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((arg0)), jni.Value((arg1)))
}

func (e *Java_lang_IntegerInstance) DoubleValue() (ret float64, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "doubleValue", "()D")
	return jni.CallDoubleMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Java_lang_IntegerInstance) Equals(arg0 jni.Object) (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "equals", "(Ljava/lang/Object;)Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Java_lang_IntegerInstance) FloatValue() (ret float32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "floatValue", "()F")
	return jni.CallFloatMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Java_lang_IntegerClass) GetInteger(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 1 && strings.HasPrefix(types[0], "java/lang/String"):
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "getInteger", "(Ljava/lang/String;)Ljava/lang/Integer;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && types[1] == "int32":
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "getInteger", "(Ljava/lang/String;I)Ljava/lang/Integer;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "java/lang/Integer"):
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "getInteger", "(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Java_lang_IntegerClass) HashCode(arg0 int32) (ret int32, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "hashCode", "(I)I")
	return jni.CallStaticIntMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((arg0)))
}

func (e *Java_lang_IntegerInstance) HashCode() (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "hashCode", "()I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Java_lang_IntegerClass) HighestOneBit(arg0 int32) (ret int32, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "highestOneBit", "(I)I")
	return jni.CallStaticIntMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((arg0)))
}

func (e *Java_lang_IntegerInstance) IntValue() (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "intValue", "()I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Java_lang_IntegerClass) Java_lang_Integer(args ...interface{}) (*Java_lang_IntegerInstance, error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return nil, err
	}
	switch {
	case len(args) == 1 && types[0] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "(I)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value((args[0].(int32))))
		if err != nil {
			return nil, err
		}
		return Java_lang_Integer_Instance(e.vmc, obj), nil

	case len(args) == 1 && strings.HasPrefix(types[0], "java/lang/String"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "(Ljava/lang/String;)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))))
		if err != nil {
			return nil, err
		}
		return Java_lang_Integer_Instance(e.vmc, obj), nil
default:
		return nil, overloadError(types)
	}
}

func (e *Java_lang_IntegerInstance) LongValue() (ret int64, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "longValue", "()J")
	return jni.CallLongMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Java_lang_IntegerClass) LowestOneBit(arg0 int32) (ret int32, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "lowestOneBit", "(I)I")
	return jni.CallStaticIntMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((arg0)))
}

func (e *Java_lang_IntegerClass) Max(arg0 int32, arg1 int32) (ret int32, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "max", "(II)I")
	return jni.CallStaticIntMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((arg0)), jni.Value((arg1)))
}

func (e *Java_lang_IntegerClass) Min(arg0 int32, arg1 int32) (ret int32, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "min", "(II)I")
	return jni.CallStaticIntMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((arg0)), jni.Value((arg1)))
}

func (e *Java_lang_IntegerClass) NumberOfLeadingZeros(arg0 int32) (ret int32, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "numberOfLeadingZeros", "(I)I")
	return jni.CallStaticIntMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((arg0)))
}

func (e *Java_lang_IntegerClass) NumberOfTrailingZeros(arg0 int32) (ret int32, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "numberOfTrailingZeros", "(I)I")
	return jni.CallStaticIntMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((arg0)))
}

func (e *Java_lang_IntegerClass) ParseInt(args ...interface{}) (ret int32, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && types[1] == "int32":
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "parseInt", "(Ljava/lang/String;I)I")
		return jni.CallStaticIntMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))))
	case len(args) == 1 && strings.HasPrefix(types[0], "java/lang/String"):
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "parseInt", "(Ljava/lang/String;)I")
		return jni.CallStaticIntMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Java_lang_IntegerClass) ParseUnsignedInt(args ...interface{}) (ret int32, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && types[1] == "int32":
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "parseUnsignedInt", "(Ljava/lang/String;I)I")
		return jni.CallStaticIntMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))))
	case len(args) == 1 && strings.HasPrefix(types[0], "java/lang/String"):
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "parseUnsignedInt", "(Ljava/lang/String;)I")
		return jni.CallStaticIntMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Java_lang_IntegerClass) RemainderUnsigned(arg0 int32, arg1 int32) (ret int32, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "remainderUnsigned", "(II)I")
	return jni.CallStaticIntMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((arg0)), jni.Value((arg1)))
}

func (e *Java_lang_IntegerClass) Reverse(arg0 int32) (ret int32, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "reverse", "(I)I")
	return jni.CallStaticIntMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((arg0)))
}

func (e *Java_lang_IntegerClass) ReverseBytes(arg0 int32) (ret int32, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "reverseBytes", "(I)I")
	return jni.CallStaticIntMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((arg0)))
}

func (e *Java_lang_IntegerClass) RotateLeft(arg0 int32, arg1 int32) (ret int32, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "rotateLeft", "(II)I")
	return jni.CallStaticIntMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((arg0)), jni.Value((arg1)))
}

func (e *Java_lang_IntegerClass) RotateRight(arg0 int32, arg1 int32) (ret int32, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "rotateRight", "(II)I")
	return jni.CallStaticIntMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((arg0)), jni.Value((arg1)))
}

func (e *Java_lang_IntegerInstance) ShortValue() (ret int16, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "shortValue", "()S")
	return jni.CallShortMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Java_lang_IntegerClass) Signum(arg0 int32) (ret int32, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "signum", "(I)I")
	return jni.CallStaticIntMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((arg0)))
}

func (e *Java_lang_IntegerClass) Sum(arg0 int32, arg1 int32) (ret int32, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "sum", "(II)I")
	return jni.CallStaticIntMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((arg0)), jni.Value((arg1)))
}

func (e *Java_lang_IntegerClass) ToBinaryString(arg0 int32) (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "toBinaryString", "(I)Ljava/lang/String;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((arg0)))
}

func (e *Java_lang_IntegerClass) ToHexString(arg0 int32) (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "toHexString", "(I)Ljava/lang/String;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((arg0)))
}

func (e *Java_lang_IntegerClass) ToOctalString(arg0 int32) (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "toOctalString", "(I)Ljava/lang/String;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((arg0)))
}

func (e *Java_lang_IntegerClass) ToString(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 2 && types[0] == "int32" && types[1] == "int32":
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "toString", "(II)Ljava/lang/String;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((args[0].(int32))), jni.Value((args[1].(int32))))
	case len(args) == 1 && types[0] == "int32":
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "toString", "(I)Ljava/lang/String;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((args[0].(int32))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Java_lang_IntegerInstance) ToString() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "toString", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Java_lang_IntegerClass) ToUnsignedLong(arg0 int32) (ret int64, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "toUnsignedLong", "(I)J")
	return jni.CallStaticLongMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((arg0)))
}

func (e *Java_lang_IntegerClass) ToUnsignedString(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 2 && types[0] == "int32" && types[1] == "int32":
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "toUnsignedString", "(II)Ljava/lang/String;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((args[0].(int32))), jni.Value((args[1].(int32))))
	case len(args) == 1 && types[0] == "int32":
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "toUnsignedString", "(I)Ljava/lang/String;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((args[0].(int32))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Java_lang_IntegerClass) ValueOf(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && types[1] == "int32":
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "valueOf", "(Ljava/lang/String;I)Ljava/lang/Integer;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))))
	case len(args) == 1 && strings.HasPrefix(types[0], "java/lang/String"):
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "valueOf", "(Ljava/lang/String;)Ljava/lang/Integer;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 1 && types[0] == "int32":
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "valueOf", "(I)Ljava/lang/Integer;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((args[0].(int32))))
	default:
		err = overloadError(types)
		return
	}
}

