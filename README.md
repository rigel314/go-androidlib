go-androidlib
======

## About
This project aims to make it easier to make pure-go Android apps.  It does this by creating golang bindings for whole android/java classes.  The currently supported classes are listed [here](generate_comments.go).  The current bindings are the ones I'm using in my stuff.  This project makes heavy use of "git.wow.st/gmp/jni" for interacting with the JNI.

## Example
TODO
