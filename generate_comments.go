// Package androidlib provides golang bindings for some android/java classes
package androidlib

//go:generate go run ./gen/genClass -androidjar android.jar -classfile android/content/ComponentName
//go:generate go run ./gen/genClass -androidjar android.jar -classfile android/content/Context
//go:generate go run ./gen/genClass -androidjar android.jar -classfile android/content/Intent
//go:generate go run ./gen/genClass -androidjar android.jar -classfile android/hardware/camera2/CameraCharacteristics
//go:generate go run ./gen/genClass -androidjar android.jar -classfile android/hardware/camera2/CameraManager
//go:generate go run ./gen/genClass -androidjar android.jar -classfile android/hardware/camera2/CameraMetadata
//go:generate go run ./gen/genClass -androidjar android.jar -classfile android/os/Environment
//go:generate go run ./gen/genClass -androidjar android.jar -classfile android/os/VibrationEffect
//go:generate go run ./gen/genClass -androidjar android.jar -classfile android/os/Vibrator
//go:generate go run ./gen/genClass -androidjar android.jar -classfile java/lang/ClassLoader
//go:generate go run ./gen/genClass -androidjar android.jar -classfile java/lang/Integer
//go:generate go run ./gen/genClass -androidjar android.jar -classfile java/lang/Object
//go:generate go run ./gen/genClass -androidjar android.jar -classfile java/lang/String
