package androidlib

import (
	"math"
	"strings"

	"git.wow.st/gmp/jni"
)

var _ = math.Float32bits
var _ = strings.HasPrefix

type Android_os_EnvironmentClass struct {
	vmc   *VMContext
	Class jni.Class
}

type Android_os_EnvironmentInstance struct {
	*Android_os_EnvironmentClass
	Jobj jni.Object
}

func Android_os_Environment(vmc *VMContext) *Android_os_EnvironmentClass {
	return &Android_os_EnvironmentClass{
		vmc:   vmc,
		Class: jni.FindClass(vmc.JNIEnv, "android/os/Environment"),
	}
}

func Android_os_Environment_Instance(vmc *VMContext, jobj jni.Object) *Android_os_EnvironmentInstance {
	return &Android_os_EnvironmentInstance{
		Android_os_EnvironmentClass: Android_os_Environment(vmc),
		Jobj: jobj,
	}
}

func (e *Android_os_EnvironmentClass) DIRECTORY_ALARMS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DIRECTORY_ALARMS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) DIRECTORY_AUDIOBOOKS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DIRECTORY_AUDIOBOOKS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) DIRECTORY_DCIM() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DIRECTORY_DCIM", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) DIRECTORY_DOCUMENTS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DIRECTORY_DOCUMENTS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) DIRECTORY_DOWNLOADS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DIRECTORY_DOWNLOADS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) DIRECTORY_MOVIES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DIRECTORY_MOVIES", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) DIRECTORY_MUSIC() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DIRECTORY_MUSIC", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) DIRECTORY_NOTIFICATIONS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DIRECTORY_NOTIFICATIONS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) DIRECTORY_PICTURES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DIRECTORY_PICTURES", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) DIRECTORY_PODCASTS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DIRECTORY_PODCASTS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) DIRECTORY_RECORDINGS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DIRECTORY_RECORDINGS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) DIRECTORY_RINGTONES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DIRECTORY_RINGTONES", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) DIRECTORY_SCREENSHOTS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DIRECTORY_SCREENSHOTS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) MEDIA_BAD_REMOVAL() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MEDIA_BAD_REMOVAL", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) MEDIA_CHECKING() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MEDIA_CHECKING", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) MEDIA_EJECTING() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MEDIA_EJECTING", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) MEDIA_MOUNTED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MEDIA_MOUNTED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) MEDIA_MOUNTED_READ_ONLY() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MEDIA_MOUNTED_READ_ONLY", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) MEDIA_NOFS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MEDIA_NOFS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) MEDIA_REMOVED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MEDIA_REMOVED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) MEDIA_SHARED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MEDIA_SHARED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) MEDIA_UNKNOWN() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MEDIA_UNKNOWN", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) MEDIA_UNMOUNTABLE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MEDIA_UNMOUNTABLE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) MEDIA_UNMOUNTED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "MEDIA_UNMOUNTED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) Android_os_Environment() (*Android_os_EnvironmentInstance, error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "()V")
	obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id)
	if err != nil {
		return nil, err
	}
	return Android_os_Environment_Instance(e.vmc, obj), nil
}

func (e *Android_os_EnvironmentClass) GetDataDirectory() (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "getDataDirectory", "()Ljava/io/File;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) GetDownloadCacheDirectory() (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "getDownloadCacheDirectory", "()Ljava/io/File;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) GetExternalStorageDirectory() (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "getExternalStorageDirectory", "()Ljava/io/File;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) GetExternalStoragePublicDirectory(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "getExternalStoragePublicDirectory", "(Ljava/lang/String;)Ljava/io/File;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_os_EnvironmentClass) GetExternalStorageState(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 0:
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "getExternalStorageState", "()Ljava/lang/String;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id)
	case len(args) == 1 && strings.HasPrefix(types[0], "java/io/File"):
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "getExternalStorageState", "(Ljava/io/File;)Ljava/lang/String;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_os_EnvironmentClass) GetRootDirectory() (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "getRootDirectory", "()Ljava/io/File;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) GetStorageDirectory() (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "getStorageDirectory", "()Ljava/io/File;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_EnvironmentClass) GetStorageState(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "getStorageState", "(Ljava/io/File;)Ljava/lang/String;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_os_EnvironmentClass) IsExternalStorageEmulated(args ...interface{}) (ret bool, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 0:
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "isExternalStorageEmulated", "()Z")
		return jni.CallStaticBooleanMethod(e.vmc.JNIEnv, e.Class, id)
	case len(args) == 1 && strings.HasPrefix(types[0], "java/io/File"):
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "isExternalStorageEmulated", "(Ljava/io/File;)Z")
		return jni.CallStaticBooleanMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_os_EnvironmentClass) IsExternalStorageLegacy(args ...interface{}) (ret bool, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 0:
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "isExternalStorageLegacy", "()Z")
		return jni.CallStaticBooleanMethod(e.vmc.JNIEnv, e.Class, id)
	case len(args) == 1 && strings.HasPrefix(types[0], "java/io/File"):
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "isExternalStorageLegacy", "(Ljava/io/File;)Z")
		return jni.CallStaticBooleanMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_os_EnvironmentClass) IsExternalStorageManager(args ...interface{}) (ret bool, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 0:
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "isExternalStorageManager", "()Z")
		return jni.CallStaticBooleanMethod(e.vmc.JNIEnv, e.Class, id)
	case len(args) == 1 && strings.HasPrefix(types[0], "java/io/File"):
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "isExternalStorageManager", "(Ljava/io/File;)Z")
		return jni.CallStaticBooleanMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_os_EnvironmentClass) IsExternalStorageRemovable(args ...interface{}) (ret bool, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 0:
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "isExternalStorageRemovable", "()Z")
		return jni.CallStaticBooleanMethod(e.vmc.JNIEnv, e.Class, id)
	case len(args) == 1 && strings.HasPrefix(types[0], "java/io/File"):
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "isExternalStorageRemovable", "(Ljava/io/File;)Z")
		return jni.CallStaticBooleanMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

