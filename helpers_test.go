package androidlib

import (
	"testing"
	"unsafe"

	"git.wow.st/gmp/jni"
)

func TestObjVal(t *testing.T) {
	v := jni.Object(1)
	if objVal(v) != 1 {
		t.Fail()
	}
	v = jni.Object(uintptr(unsafe.Pointer(&v)))
	if objVal(v) != uintptr(unsafe.Pointer(&v)) {
		t.Fail()
	}
}
