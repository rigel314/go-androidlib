package main

import (
	"bufio"
	"bytes"
	_ "embed"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"regexp"
	"sort"
	"strings"
	"text/template"
)

var (
	androidJar = flag.String("androidjar", "", "path to android.jar")
	classFile  = flag.String("classfile", "", "path inside android.jar to class file, e.g. android/os/Environment.class")
	javapPath  = flag.String("javappath", "javap", "path to javap")
)

//go:generate stringer -type=MemberKind
type MemberKind int

const (
	InvalidMemberKind MemberKind = iota
	FieldKind
	ConstructorKind
	MethodKind
)

type object struct {
	Kind             MemberKind
	Static           bool
	Bridge           []bool
	Name             string
	Descriptor       []string
	ParsedDescriptor []*descriptor
}

//go:embed class.gotmpl
var tmpl string

type objectMapKey struct {
	Name   string
	Static bool
}

type objectMap map[objectMapKey]*object

type tmplateType struct {
	ClassName  string
	ClassPath  string
	SortedKeys []objectMapKey
	Objects    objectMap
}

func main() {
	flag.Parse()

	contents := system("", *javapPath, "-v", "-public", "-s", "-cp", *androidJar, *classFile)

	var objs = make(objectMap)
	working := &object{}
	rd := bufio.NewReader(bytes.NewBuffer(contents))
	for {
		rawline, err := rd.ReadString('\n')
		if err != nil {
			if errors.Is(err, io.EOF) {
				break
			}
			panic(err)
		}
		line := strings.TrimSpace(rawline)
		// if line == "" {
		// 	continue
		// }
		// if strings.ContainsAny(line, "{}") {
		// 	continue
		// }
		// if strings.HasPrefix(line, "Compiled from ") {
		// 	continue
		// }
		switch {
		default:
			// working.Static = false
			// if regexp.MustCompile(`\bstatic\b`).MatchString(line) {
			// 	working.Static = true
			// }
			// if regexp.MustCompile(`\bthrows\b`).MatchString(line) {
			// 	fmt.Println("throws", line)
			// }
			methodName := regexp.MustCompile(`^  public.* ([a-zA-Z_]\w+)\(`).FindStringSubmatch(rawline)
			constructorName := regexp.MustCompile(`^  public.* ((?:[a-zA-Z_][0-9a-zA-Z_]+.?)+)\(`).FindStringSubmatch(rawline)
			fieldName := regexp.MustCompile(`^  public.* ([a-zA-Z_]\w+);`).FindStringSubmatch(rawline)
			switch {
			case methodName != nil:
				working.Kind = MethodKind
				working.Name = methodName[1]
			case constructorName != nil:
				working.Kind = ConstructorKind
				working.Name = constructorName[1]
			case fieldName != nil:
				working.Kind = FieldKind
				working.Name = fieldName[1]
			}
		case strings.HasPrefix(rawline, "    descriptor: "):
			splits := strings.Split(line, " ")
			descriptor := splits[1]
			working.Descriptor = append(working.Descriptor, descriptor)
			working.ParsedDescriptor = append(working.ParsedDescriptor, ParseDescriptor(descriptor))
		case strings.HasPrefix(rawline, "    flags: "):
			splits := strings.SplitN(line, " ", 2)
			flagsStr := splits[1]
			flags := strings.Split(flagsStr, ", ")
			br := false
			for _, flag := range flags {
				switch flag {
				case "ACC_STATIC":
					working.Static = true
				case "ACC_BRIDGE":
					br = true
				}
			}
			working.Bridge = append(working.Bridge, br)

			key := objectMapKey{Name: working.Name, Static: working.Static}

			if _, ok := objs[key]; !ok {
				objs[key] = working
			} else {
				v := objs[key]
				v.Bridge = append(v.Bridge, working.Bridge...)
				v.Descriptor = append(v.Descriptor, working.Descriptor...)
				v.ParsedDescriptor = append(v.ParsedDescriptor, working.ParsedDescriptor...)
				objs[key] = v
			}

			working = &object{}
		}
	}

	keys := make([]objectMapKey, 0, len(objs))
	for k := range objs {
		keys = append(keys, k)
	}
	sort.Slice(keys, func(i, j int) bool {
		if keys[i].Name < keys[j].Name {
			return true
		}
		if keys[i].Name > keys[j].Name {
			return false
		}
		return keys[i].Static && !keys[j].Static
	})

	// sanity check on return types for overloads
	for _, k := range keys {
		if objs[k].Kind == FieldKind {
			continue
		}
		m := make(map[string][]*object)
		for _, v := range objs[k].ParsedDescriptor {
			s := ""
			for _, d := range v.Elems {
				s += d.DescType.RawType
			}
			l := m[s]
			l = append(l, objs[k])
			m[s] = l
		}
		for sig, v := range m {
			if len(v) > 1 {
				var s []string
				for _, ret := range v[0].ParsedDescriptor {
					s = append(s, ret.Ret.DescType.RawType)
				}
				fmt.Println("covariant returns for", k.Name, "(", sig, ")", ":", s)
				var idxs []int
				for i, br := range v[0].Bridge {
					if br {
						idxs = append(idxs, i)
					}
				}
				// reverse idxs
				for i, j := 0, len(idxs)-1; i < j; i, j = i+1, j-1 {
					idxs[i], idxs[j] = idxs[j], idxs[i]
				}
				for _, i := range idxs {
					fmt.Println("  deleting bridge:", v[0].ParsedDescriptor[i].Ret.DescType.RawType)
					v[0].Bridge = append(v[0].Bridge[:i], v[0].Bridge[i+1:]...)
					v[0].Descriptor = append(v[0].Descriptor[:i], v[0].Descriptor[i+1:]...)
					v[0].ParsedDescriptor = append(v[0].ParsedDescriptor[:i], v[0].ParsedDescriptor[i+1:]...)
				}
			}
		}
	}

	tt := tmplateType{
		ClassName:  strings.ToUpper((*classFile)[0:1]) + strings.ReplaceAll(*classFile, "/", "_")[1:],
		ClassPath:  *classFile,
		Objects:    objs,
		SortedKeys: keys,
	}

	t, err := template.New("").Funcs(template.FuncMap{
		"public": func(t object) string {
			s := t.Name
			s = strings.ToUpper(s[0:1]) + s[1:]
			s = strings.ReplaceAll(s, ".", "_")
			return s
		},
		"isMethod": func(k MemberKind) bool {
			return k == MethodKind
		},
		"isField": func(k MemberKind) bool {
			return k == FieldKind
		},
		"isConstructor": func(k MemberKind) bool {
			return k == ConstructorKind
		},
		"goArgNameTypes": func(t object, idx int) string {
			d := t.ParsedDescriptor[idx]
			argStr := ""
			for i, t := range d.Elems {
				comma := ", "
				if i == 0 {
					comma = ""
				}
				argStr += fmt.Sprintf("%sarg%d %s", comma, i, descKindToGoType(t.DescType.Kind, true))
			}
			return argStr
		},
		"jniArgsCast": func(t object, idx int, indexArgs bool) string {
			d := t.ParsedDescriptor[idx]
			argStr := ""
			for i, t := range d.Elems {
				cast := ""
				switch t.DescType.Kind {
				case BooleanKind:
					cast = "Boole"
				case ByteKind, CharKind, ShortKind, IntKind, LongKind:
					cast = ""
				case FloatKind:
					cast = "math.Float32bits"
				case DoubleKind:
					cast = "math.Float64bits"
				case ObjectKind, ArrayKind, FuncKind:
					cast = "ObjVal"
				case VoidKind:
					panic("invalid void")
				default:
					panic("unhandled type: " + t.DescType.Kind.String())
				}
				openbrack, closebrack := "", ""
				if indexArgs {
					openbrack = "s["
					closebrack = fmt.Sprintf("].(%s)", descKindToGoType(t.DescType.Kind, true))
				}
				argStr += fmt.Sprintf(", jni.Value(%s(arg%s%d%s))", cast, openbrack, i, closebrack)
			}
			return argStr
		},
		"isVoidOverloadedRet": func(t object, idx int) bool {
			var k DescKind = t.ParsedDescriptor[0].Ret.DescType.Kind
			for _, d := range t.ParsedDescriptor {
				if d.Ret.DescType.Kind != k {
					return t.ParsedDescriptor[idx].Ret.DescType.Kind == VoidKind
				}
			}
			return false
		},
		"goReturnType": func(t object) string {
			var k DescKind = t.ParsedDescriptor[0].Ret.DescType.Kind
			for _, d := range t.ParsedDescriptor {
				if d.Ret.DescType.Kind != k {
					return "ret interface{}, "
				}
			}
			switch k {
			case BooleanKind:
				return "ret bool, "
			case ByteKind:
				return "ret byte, "
			case CharKind:
				return "ret rune, "
			case ShortKind:
				return "ret int16, "
			case IntKind:
				return "ret int32, "
			case LongKind:
				return "ret int64, "
			case FloatKind:
				return "ret float32, "
			case DoubleKind:
				return "ret float64, "
			case ObjectKind:
				return "ret jni.Object, "
			case ArrayKind:
				return "ret jni.Object, "
			case VoidKind:
				return ""
			case FuncKind:
				return "ret jni.Object, "
			default:
				panic("unhandled type: " + k.String())
			}
		},
		"goType": func(t object, idx int) string {
			return descKindToGoType(t.ParsedDescriptor[idx].DescType.Kind, true)
		},
		"javaCallMethodType": func(t object, idx int) string {
			d := t.ParsedDescriptor[idx]
			return descKindToJavaType(d.Ret.DescType.Kind)
		},
		"javaType": func(t object, idx int) string {
			d := t.ParsedDescriptor[idx]
			return descKindToJavaType(d.DescType.Kind)
		},
		"overloadTypeChecks": func(t object, idx int, typevar string) string {
			d := t.ParsedDescriptor[idx]
			ret := fmt.Sprintf("%d", len(d.Elems))
			for i, v := range d.Elems {
				typ := descKindToGoType(v.DescType.Kind, true)
				if typ == "jni.Object" {
					typ = v.DescType.RawType
					if typ[0] == 'L' && typ[len(typ)-1] == ';' && strings.Count(typ, ";") == 1 {
						typ = typ[1 : len(typ)-1]
					}
					ret += fmt.Sprintf(` && strings.HasPrefix(%s[%d], "%s")`, typevar, i, typ)
				} else {
					ret += fmt.Sprintf(` && %s[%d] == "%s"`, typevar, i, typ)
				}
			}
			return ret
		},
	}).Parse(tmpl)
	if err != nil {
		panic(err)
	}
	f, err := os.Create("xlib_" + tt.ClassName + "_gen.go")
	if err != nil {
		panic(err)
	}
	defer f.Close()
	// f = os.Stdout
	err = t.Execute(f, tt)
	if err != nil {
		panic(err)
	}
}

// func hash(desc string) string {
// 	sum := md5.Sum([]byte(desc))
// 	return hex.EncodeToString(sum[:])[:6]
// }

//go:generate stringer -type=DescKind
type DescKind int

const (
	InvalidDescKind DescKind = iota
	BooleanKind
	ByteKind
	CharKind
	ShortKind
	IntKind
	LongKind
	FloatKind
	DoubleKind
	ObjectKind
	ArrayKind
	VoidKind
	FuncKind
)

func descKindToGoType(k DescKind, ret bool) string {
	obj := "jni.Value"
	if ret {
		obj = "jni.Object"
	}
	switch k {
	case BooleanKind:
		return "bool"
	case ByteKind:
		return "byte"
	case CharKind:
		return "rune"
	case ShortKind:
		return "int16"
	case IntKind:
		return "int32"
	case LongKind:
		return "int64"
	case FloatKind:
		return "float32"
	case DoubleKind:
		return "float64"
	case ObjectKind:
		return obj
	case ArrayKind:
		return obj
	case VoidKind:
		return ""
	case FuncKind:
		return obj
	default:
		panic("unhandled type: " + k.String())
	}
}
func descKindToJavaType(k DescKind) string {
	switch k {
	case BooleanKind:
		return "Boolean"
	case ByteKind:
		return "Byte"
	case CharKind:
		return "Char"
	case ShortKind:
		return "Short"
	case IntKind:
		return "Int"
	case LongKind:
		return "Long"
	case FloatKind:
		return "Float"
	case DoubleKind:
		return "Double"
	case ObjectKind:
		return "Object"
	case ArrayKind:
		return "Object"
	case VoidKind:
		return "Void"
	case FuncKind:
		return "Object"
	default:
		panic("unhandled type: " + k.String())
	}
}

type DescType struct {
	Kind    DescKind
	RawType string
}
type descriptor struct {
	DescType *DescType
	Elems    []*descriptor
	Ret      *descriptor
}

func ParseDescriptor(desc string) *descriptor {
	ret := &descriptor{}

	rd := strings.NewReader(desc)
	ch, _, err := rd.ReadRune()
	if err != nil {
		panic(err)
	}
	switch ch {
	case 'Z':
		ret.DescType = &DescType{Kind: BooleanKind, RawType: string(ch)}
	case 'B':
		ret.DescType = &DescType{Kind: ByteKind, RawType: string(ch)}
	case 'C':
		ret.DescType = &DescType{Kind: CharKind, RawType: string(ch)}
	case 'S':
		ret.DescType = &DescType{Kind: ShortKind, RawType: string(ch)}
	case 'I':
		ret.DescType = &DescType{Kind: IntKind, RawType: string(ch)}
	case 'J':
		ret.DescType = &DescType{Kind: LongKind, RawType: string(ch)}
	case 'F':
		ret.DescType = &DescType{Kind: FloatKind, RawType: string(ch)}
	case 'D':
		ret.DescType = &DescType{Kind: DoubleKind, RawType: string(ch)}
	case 'V':
		ret.DescType = &DescType{Kind: VoidKind, RawType: string(ch)}
	case 'L':
		brd := bufio.NewReader(strings.NewReader(desc))
		s, err := brd.ReadString(';')
		if err != nil {
			panic(err)
		}
		ret.DescType = &DescType{Kind: ObjectKind, RawType: s}
	case '[':
		arrType := ParseDescriptor(desc[1:])
		ret.DescType = &DescType{Kind: ArrayKind, RawType: string(ch) + arrType.DescType.RawType}
		ret.Elems = append(ret.Elems, arrType)
	case '(':
		i := 1
		for {
			if desc[i] == ')' {
				i++
				break
			}
			argType := ParseDescriptor(desc[i:])
			ret.Elems = append(ret.Elems, argType)
			i += len(argType.DescType.RawType)
		}
		retType := ParseDescriptor(desc[i:])
		ret.Ret = retType
		ret.DescType = &DescType{Kind: FuncKind, RawType: string(ch)}
	default:
		panic(fmt.Sprintf("unhandled case: at %q, when parsing %q", ch, desc))
	}

	return ret
}

func system(dir string, name string, args ...string) []byte {
	cmd := exec.Command(name, args...)
	cmd.Dir = dir
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	out, err := cmd.StdoutPipe()
	if err != nil {
		panic(err)
	}
	err = cmd.Start()
	if err != nil {
		panic(err)
	}
	ret, err := ioutil.ReadAll(out)
	if err != nil {
		panic(err)
	}
	err = cmd.Wait()
	if err != nil {
		panic(err)
	}

	return ret
}
