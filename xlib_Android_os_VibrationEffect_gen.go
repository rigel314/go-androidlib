package androidlib

import (
	"math"
	"strings"

	"git.wow.st/gmp/jni"
)

var _ = math.Float32bits
var _ = strings.HasPrefix

type Android_os_VibrationEffectClass struct {
	vmc   *VMContext
	Class jni.Class
}

type Android_os_VibrationEffectInstance struct {
	*Android_os_VibrationEffectClass
	Jobj jni.Object
}

func Android_os_VibrationEffect(vmc *VMContext) *Android_os_VibrationEffectClass {
	return &Android_os_VibrationEffectClass{
		vmc:   vmc,
		Class: jni.FindClass(vmc.JNIEnv, "android/os/VibrationEffect"),
	}
}

func Android_os_VibrationEffect_Instance(vmc *VMContext, jobj jni.Object) *Android_os_VibrationEffectInstance {
	return &Android_os_VibrationEffectInstance{
		Android_os_VibrationEffectClass: Android_os_VibrationEffect(vmc),
		Jobj: jobj,
	}
}

func (e *Android_os_VibrationEffectClass) CREATOR() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CREATOR", "Landroid/os/Parcelable$Creator;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_VibrationEffectClass) DEFAULT_AMPLITUDE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DEFAULT_AMPLITUDE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_VibrationEffectClass) EFFECT_CLICK() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EFFECT_CLICK", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_VibrationEffectClass) EFFECT_DOUBLE_CLICK() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EFFECT_DOUBLE_CLICK", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_VibrationEffectClass) EFFECT_HEAVY_CLICK() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EFFECT_HEAVY_CLICK", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_VibrationEffectClass) EFFECT_TICK() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EFFECT_TICK", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_os_VibrationEffectClass) CreateOneShot(arg0 int64, arg1 int32) (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "createOneShot", "(JI)Landroid/os/VibrationEffect;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((arg0)), jni.Value((arg1)))
}

func (e *Android_os_VibrationEffectClass) CreatePredefined(arg0 int32) (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "createPredefined", "(I)Landroid/os/VibrationEffect;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((arg0)))
}

func (e *Android_os_VibrationEffectClass) CreateWaveform(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 2 && strings.HasPrefix(types[0], "[J") && types[1] == "int32":
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "createWaveform", "([JI)Landroid/os/VibrationEffect;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))))
	case len(args) == 3 && strings.HasPrefix(types[0], "[J") && strings.HasPrefix(types[1], "[I") && types[2] == "int32":
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "createWaveform", "([J[II)Landroid/os/VibrationEffect;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))), jni.Value((args[2].(int32))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_os_VibrationEffectInstance) DescribeContents() (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "describeContents", "()I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_os_VibrationEffectClass) StartComposition() (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "startComposition", "()Landroid/os/VibrationEffect$Composition;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id)
}

