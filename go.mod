module gitlab.com/rigel314/go-androidlib

go 1.18

require git.wow.st/gmp/jni v0.0.0-20210610011705-34026c7e22d0

// replace git.wow.st/gmp/jni => gitlab.com/rigel314/go-jni v0.0.0-20220523051044-07a3472a6b30

replace git.wow.st/gmp/jni => ../jni
