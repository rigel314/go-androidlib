package androidlib

import (
	"fmt"
	"strings"
	"unsafe"

	"git.wow.st/gmp/jni"
)

var NoSuchOverload = fmt.Errorf("no such overload")

func overloadError(types []string) error {
	return fmt.Errorf("%w: [%s]", NoSuchOverload, strings.Join(types, ","))
}

func Boole(b bool) int {
	if b {
		return 1
	}
	return 0
}

func ObjVal(o jni.Object) uintptr {
	return uintptr(*(*unsafe.Pointer)(unsafe.Pointer(&o)))
}

func UintptrToObject(p uintptr) jni.Object {
	return *(*jni.Object)(unsafe.Pointer(&p))
}

func StringToObject(vmc *VMContext, s string) jni.Object {
	return jni.Object(jni.JavaString(vmc.JNIEnv, s))
}

func ObjectClassName(vmc *VMContext, o jni.Object) (string, error) {
	inst_class := jni.GetObjectClass(vmc.JNIEnv, o)
	return ClassName(vmc, inst_class)

}

func ClassName(vmc *VMContext, c jni.Class) (string, error) {
	class_class := jni.FindClass(vmc.JNIEnv, "java/lang/Class")
	id := jni.GetMethodID(vmc.JNIEnv, class_class, "getName", "()Ljava/lang/String;")
	jname, err := jni.CallObjectMethod(vmc.JNIEnv, jni.Object(c), id)
	if err != nil {
		return "", err
	}
	ret := jni.GoString(vmc.JNIEnv, jni.String(jname))
	ret = strings.ReplaceAll(ret, ".", "/")
	return ret, nil
}

func ArgTypes(vmc *VMContext, args ...interface{}) ([]string, error) {
	types := make([]string, 0, len(args))
	for _, v := range args {
		switch o := v.(type) {
		case bool:
			types = append(types, "bool")
		case byte:
			types = append(types, "byte")
		// case rune: // rune is just an alias for int32
		// 	types = append(types, "rune")
		case int16:
			types = append(types, "int16")
		case int32:
			types = append(types, "int32")
		case int64:
			types = append(types, "int64")
		case float32:
			types = append(types, "float32")
		case float64:
			types = append(types, "float64")
		case jni.Object:
			name, err := ObjectClassName(vmc, o)
			if err != nil {
				return nil, err
			}
			types = append(types, name)
		case jni.Class:
			name, err := ClassName(vmc, o)
			if err != nil {
				return nil, err
			}
			types = append(types, name)
		default:
			return nil, fmt.Errorf("unhandled type: %T", v)
		}
	}

	return types, nil
}

type LoopControl int

const (
	Continue LoopControl = iota
	Break    LoopControl = iota
)

func ForEach(env *VMContext, oa jni.ObjectArray, fn func(i int, v jni.Object) (LoopControl, error)) error {
	max := jni.GetArrayLength(env.JNIEnv, jni.Object(oa))
	for i := 0; i < max; i++ {
		o, err := jni.GetObjectArrayElement(env.JNIEnv, oa, jni.Size(i))
		if err != nil {
			return err
		}
		lc, err := fn(i, o)
		if err != nil {
			return err
		}
		if lc == Break {
			break
		}
	}
	return nil
}

func ArrayObjects(env *VMContext, oa jni.ObjectArray) ([]jni.Object, error) {
	max := jni.GetArrayLength(env.JNIEnv, jni.Object(oa))
	ret := make([]jni.Object, 0, max)
	for i := 0; i < max; i++ {
		o, err := jni.GetObjectArrayElement(env.JNIEnv, oa, jni.Size(i))
		if err != nil {
			return nil, err
		}
		ret = append(ret, o)
	}
	return ret, nil
}
