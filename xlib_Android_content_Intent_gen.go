package androidlib

import (
	"math"
	"strings"

	"git.wow.st/gmp/jni"
)

var _ = math.Float32bits
var _ = strings.HasPrefix

type Android_content_IntentClass struct {
	vmc   *VMContext
	Class jni.Class
}

type Android_content_IntentInstance struct {
	*Android_content_IntentClass
	Jobj jni.Object
}

func Android_content_Intent(vmc *VMContext) *Android_content_IntentClass {
	return &Android_content_IntentClass{
		vmc:   vmc,
		Class: jni.FindClass(vmc.JNIEnv, "android/content/Intent"),
	}
}

func Android_content_Intent_Instance(vmc *VMContext, jobj jni.Object) *Android_content_IntentInstance {
	return &Android_content_IntentInstance{
		Android_content_IntentClass: Android_content_Intent(vmc),
		Jobj: jobj,
	}
}

func (e *Android_content_IntentClass) ACTION_AIRPLANE_MODE_CHANGED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_AIRPLANE_MODE_CHANGED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_ALL_APPS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_ALL_APPS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_ANSWER() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_ANSWER", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_APPLICATION_PREFERENCES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_APPLICATION_PREFERENCES", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_APPLICATION_RESTRICTIONS_CHANGED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_APPLICATION_RESTRICTIONS_CHANGED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_APP_ERROR() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_APP_ERROR", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_ASSIST() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_ASSIST", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_ATTACH_DATA() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_ATTACH_DATA", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_AUTO_REVOKE_PERMISSIONS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_AUTO_REVOKE_PERMISSIONS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_BATTERY_CHANGED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_BATTERY_CHANGED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_BATTERY_LOW() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_BATTERY_LOW", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_BATTERY_OKAY() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_BATTERY_OKAY", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_BOOT_COMPLETED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_BOOT_COMPLETED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_BUG_REPORT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_BUG_REPORT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_CALL() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_CALL", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_CALL_BUTTON() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_CALL_BUTTON", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_CAMERA_BUTTON() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_CAMERA_BUTTON", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_CARRIER_SETUP() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_CARRIER_SETUP", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_CHOOSER() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_CHOOSER", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_CLOSE_SYSTEM_DIALOGS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_CLOSE_SYSTEM_DIALOGS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_CONFIGURATION_CHANGED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_CONFIGURATION_CHANGED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_CREATE_DOCUMENT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_CREATE_DOCUMENT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_CREATE_REMINDER() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_CREATE_REMINDER", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_CREATE_SHORTCUT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_CREATE_SHORTCUT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_DATE_CHANGED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_DATE_CHANGED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_DEFAULT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_DEFAULT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_DEFINE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_DEFINE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_DELETE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_DELETE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_DEVICE_STORAGE_LOW() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_DEVICE_STORAGE_LOW", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_DEVICE_STORAGE_OK() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_DEVICE_STORAGE_OK", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_DIAL() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_DIAL", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_DOCK_EVENT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_DOCK_EVENT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_DREAMING_STARTED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_DREAMING_STARTED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_DREAMING_STOPPED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_DREAMING_STOPPED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_EDIT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_EDIT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_EXTERNAL_APPLICATIONS_AVAILABLE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_EXTERNAL_APPLICATIONS_AVAILABLE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_EXTERNAL_APPLICATIONS_UNAVAILABLE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_EXTERNAL_APPLICATIONS_UNAVAILABLE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_FACTORY_TEST() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_FACTORY_TEST", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_GET_CONTENT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_GET_CONTENT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_GET_RESTRICTION_ENTRIES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_GET_RESTRICTION_ENTRIES", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_GTALK_SERVICE_CONNECTED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_GTALK_SERVICE_CONNECTED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_GTALK_SERVICE_DISCONNECTED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_GTALK_SERVICE_DISCONNECTED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_HEADSET_PLUG() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_HEADSET_PLUG", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_INPUT_METHOD_CHANGED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_INPUT_METHOD_CHANGED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_INSERT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_INSERT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_INSERT_OR_EDIT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_INSERT_OR_EDIT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_INSTALL_FAILURE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_INSTALL_FAILURE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_INSTALL_PACKAGE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_INSTALL_PACKAGE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_LOCALE_CHANGED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_LOCALE_CHANGED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_LOCKED_BOOT_COMPLETED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_LOCKED_BOOT_COMPLETED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MAIN() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MAIN", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MANAGED_PROFILE_ADDED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MANAGED_PROFILE_ADDED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MANAGED_PROFILE_AVAILABLE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MANAGED_PROFILE_AVAILABLE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MANAGED_PROFILE_REMOVED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MANAGED_PROFILE_REMOVED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MANAGED_PROFILE_UNAVAILABLE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MANAGED_PROFILE_UNAVAILABLE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MANAGED_PROFILE_UNLOCKED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MANAGED_PROFILE_UNLOCKED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MANAGE_NETWORK_USAGE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MANAGE_NETWORK_USAGE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MANAGE_PACKAGE_STORAGE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MANAGE_PACKAGE_STORAGE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MANAGE_UNUSED_APPS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MANAGE_UNUSED_APPS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MEDIA_BAD_REMOVAL() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MEDIA_BAD_REMOVAL", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MEDIA_BUTTON() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MEDIA_BUTTON", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MEDIA_CHECKING() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MEDIA_CHECKING", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MEDIA_EJECT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MEDIA_EJECT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MEDIA_MOUNTED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MEDIA_MOUNTED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MEDIA_NOFS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MEDIA_NOFS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MEDIA_REMOVED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MEDIA_REMOVED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MEDIA_SCANNER_FINISHED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MEDIA_SCANNER_FINISHED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MEDIA_SCANNER_SCAN_FILE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MEDIA_SCANNER_SCAN_FILE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MEDIA_SCANNER_STARTED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MEDIA_SCANNER_STARTED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MEDIA_SHARED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MEDIA_SHARED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MEDIA_UNMOUNTABLE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MEDIA_UNMOUNTABLE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MEDIA_UNMOUNTED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MEDIA_UNMOUNTED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MY_PACKAGE_REPLACED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MY_PACKAGE_REPLACED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MY_PACKAGE_SUSPENDED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MY_PACKAGE_SUSPENDED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_MY_PACKAGE_UNSUSPENDED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_MY_PACKAGE_UNSUSPENDED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_NEW_OUTGOING_CALL() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_NEW_OUTGOING_CALL", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_OPEN_DOCUMENT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_OPEN_DOCUMENT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_OPEN_DOCUMENT_TREE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_OPEN_DOCUMENT_TREE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_PACKAGES_SUSPENDED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_PACKAGES_SUSPENDED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_PACKAGES_UNSUSPENDED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_PACKAGES_UNSUSPENDED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_PACKAGE_ADDED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_PACKAGE_ADDED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_PACKAGE_CHANGED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_PACKAGE_CHANGED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_PACKAGE_DATA_CLEARED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_PACKAGE_DATA_CLEARED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_PACKAGE_FIRST_LAUNCH() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_PACKAGE_FIRST_LAUNCH", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_PACKAGE_FULLY_REMOVED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_PACKAGE_FULLY_REMOVED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_PACKAGE_INSTALL() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_PACKAGE_INSTALL", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_PACKAGE_NEEDS_VERIFICATION() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_PACKAGE_NEEDS_VERIFICATION", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_PACKAGE_REMOVED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_PACKAGE_REMOVED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_PACKAGE_REPLACED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_PACKAGE_REPLACED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_PACKAGE_RESTARTED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_PACKAGE_RESTARTED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_PACKAGE_VERIFIED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_PACKAGE_VERIFIED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_PASTE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_PASTE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_PICK() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_PICK", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_PICK_ACTIVITY() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_PICK_ACTIVITY", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_POWER_CONNECTED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_POWER_CONNECTED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_POWER_DISCONNECTED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_POWER_DISCONNECTED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_POWER_USAGE_SUMMARY() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_POWER_USAGE_SUMMARY", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_PROCESS_TEXT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_PROCESS_TEXT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_PROFILE_ACCESSIBLE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_PROFILE_ACCESSIBLE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_PROFILE_INACCESSIBLE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_PROFILE_INACCESSIBLE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_PROVIDER_CHANGED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_PROVIDER_CHANGED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_QUICK_CLOCK() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_QUICK_CLOCK", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_QUICK_VIEW() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_QUICK_VIEW", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_REBOOT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_REBOOT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_RUN() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_RUN", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_SCREEN_OFF() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_SCREEN_OFF", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_SCREEN_ON() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_SCREEN_ON", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_SEARCH() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_SEARCH", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_SEARCH_LONG_PRESS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_SEARCH_LONG_PRESS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_SEND() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_SEND", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_SENDTO() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_SENDTO", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_SEND_MULTIPLE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_SEND_MULTIPLE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_SET_WALLPAPER() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_SET_WALLPAPER", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_SHOW_APP_INFO() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_SHOW_APP_INFO", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_SHUTDOWN() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_SHUTDOWN", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_SYNC() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_SYNC", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_SYSTEM_TUTORIAL() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_SYSTEM_TUTORIAL", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_TIMEZONE_CHANGED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_TIMEZONE_CHANGED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_TIME_CHANGED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_TIME_CHANGED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_TIME_TICK() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_TIME_TICK", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_TRANSLATE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_TRANSLATE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_UID_REMOVED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_UID_REMOVED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_UMS_CONNECTED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_UMS_CONNECTED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_UMS_DISCONNECTED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_UMS_DISCONNECTED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_UNINSTALL_PACKAGE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_UNINSTALL_PACKAGE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_USER_BACKGROUND() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_USER_BACKGROUND", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_USER_FOREGROUND() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_USER_FOREGROUND", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_USER_INITIALIZE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_USER_INITIALIZE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_USER_PRESENT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_USER_PRESENT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_USER_UNLOCKED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_USER_UNLOCKED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_VIEW() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_VIEW", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_VIEW_LOCUS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_VIEW_LOCUS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_VIEW_PERMISSION_USAGE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_VIEW_PERMISSION_USAGE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_VIEW_PERMISSION_USAGE_FOR_PERIOD() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_VIEW_PERMISSION_USAGE_FOR_PERIOD", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_VOICE_COMMAND() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_VOICE_COMMAND", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_WALLPAPER_CHANGED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_WALLPAPER_CHANGED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) ACTION_WEB_SEARCH() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "ACTION_WEB_SEARCH", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_ACCESSIBILITY_SHORTCUT_TARGET() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_ACCESSIBILITY_SHORTCUT_TARGET", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_ALTERNATIVE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_ALTERNATIVE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_APP_BROWSER() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_APP_BROWSER", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_APP_CALCULATOR() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_APP_CALCULATOR", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_APP_CALENDAR() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_APP_CALENDAR", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_APP_CONTACTS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_APP_CONTACTS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_APP_EMAIL() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_APP_EMAIL", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_APP_FILES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_APP_FILES", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_APP_GALLERY() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_APP_GALLERY", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_APP_MAPS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_APP_MAPS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_APP_MARKET() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_APP_MARKET", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_APP_MESSAGING() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_APP_MESSAGING", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_APP_MUSIC() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_APP_MUSIC", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_BROWSABLE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_BROWSABLE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_CAR_DOCK() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_CAR_DOCK", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_CAR_MODE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_CAR_MODE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_DEFAULT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_DEFAULT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_DESK_DOCK() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_DESK_DOCK", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_DEVELOPMENT_PREFERENCE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_DEVELOPMENT_PREFERENCE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_EMBED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_EMBED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_FRAMEWORK_INSTRUMENTATION_TEST() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_FRAMEWORK_INSTRUMENTATION_TEST", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_HE_DESK_DOCK() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_HE_DESK_DOCK", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_HOME() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_HOME", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_INFO() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_INFO", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_LAUNCHER() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_LAUNCHER", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_LEANBACK_LAUNCHER() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_LEANBACK_LAUNCHER", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_LE_DESK_DOCK() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_LE_DESK_DOCK", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_MONKEY() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_MONKEY", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_OPENABLE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_OPENABLE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_PREFERENCE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_PREFERENCE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_SAMPLE_CODE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_SAMPLE_CODE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_SECONDARY_HOME() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_SECONDARY_HOME", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_SELECTED_ALTERNATIVE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_SELECTED_ALTERNATIVE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_TAB() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_TAB", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_TEST() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_TEST", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_TYPED_OPENABLE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_TYPED_OPENABLE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_UNIT_TEST() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_UNIT_TEST", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_VOICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_VOICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CATEGORY_VR_HOME() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CATEGORY_VR_HOME", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) CREATOR() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CREATOR", "Landroid/os/Parcelable$Creator;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_ALARM_COUNT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_ALARM_COUNT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_ALLOW_MULTIPLE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_ALLOW_MULTIPLE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_ALLOW_REPLACE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_ALLOW_REPLACE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_ALTERNATE_INTENTS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_ALTERNATE_INTENTS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_ASSIST_CONTEXT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_ASSIST_CONTEXT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_ASSIST_INPUT_DEVICE_ID() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_ASSIST_INPUT_DEVICE_ID", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_ASSIST_INPUT_HINT_KEYBOARD() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_ASSIST_INPUT_HINT_KEYBOARD", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_ASSIST_PACKAGE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_ASSIST_PACKAGE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_ASSIST_UID() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_ASSIST_UID", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_ATTRIBUTION_TAGS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_ATTRIBUTION_TAGS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_AUTO_LAUNCH_SINGLE_CHOICE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_AUTO_LAUNCH_SINGLE_CHOICE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_BCC() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_BCC", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_BUG_REPORT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_BUG_REPORT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_CC() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_CC", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_CHANGED_COMPONENT_NAME() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_CHANGED_COMPONENT_NAME", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_CHANGED_COMPONENT_NAME_LIST() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_CHANGED_COMPONENT_NAME_LIST", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_CHANGED_PACKAGE_LIST() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_CHANGED_PACKAGE_LIST", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_CHANGED_UID_LIST() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_CHANGED_UID_LIST", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_CHOOSER_REFINEMENT_INTENT_SENDER() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_CHOOSER_REFINEMENT_INTENT_SENDER", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_CHOOSER_TARGETS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_CHOOSER_TARGETS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_CHOSEN_COMPONENT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_CHOSEN_COMPONENT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_CHOSEN_COMPONENT_INTENT_SENDER() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_CHOSEN_COMPONENT_INTENT_SENDER", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_COMPONENT_NAME() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_COMPONENT_NAME", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_CONTENT_ANNOTATIONS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_CONTENT_ANNOTATIONS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_CONTENT_QUERY() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_CONTENT_QUERY", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_DATA_REMOVED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_DATA_REMOVED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_DOCK_STATE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_DOCK_STATE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_DOCK_STATE_CAR() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_DOCK_STATE_CAR", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_DOCK_STATE_DESK() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_DOCK_STATE_DESK", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_DOCK_STATE_HE_DESK() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_DOCK_STATE_HE_DESK", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_DOCK_STATE_LE_DESK() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_DOCK_STATE_LE_DESK", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_DOCK_STATE_UNDOCKED() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_DOCK_STATE_UNDOCKED", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_DONT_KILL_APP() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_DONT_KILL_APP", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_DURATION_MILLIS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_DURATION_MILLIS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_EMAIL() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_EMAIL", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_END_TIME() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_END_TIME", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_EXCLUDE_COMPONENTS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_EXCLUDE_COMPONENTS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_FROM_STORAGE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_FROM_STORAGE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_HTML_TEXT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_HTML_TEXT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_INDEX() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_INDEX", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_INITIAL_INTENTS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_INITIAL_INTENTS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_INSTALLER_PACKAGE_NAME() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_INSTALLER_PACKAGE_NAME", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_INTENT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_INTENT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_KEY_EVENT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_KEY_EVENT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_LOCAL_ONLY() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_LOCAL_ONLY", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_LOCUS_ID() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_LOCUS_ID", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_MIME_TYPES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_MIME_TYPES", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_NOT_UNKNOWN_SOURCE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_NOT_UNKNOWN_SOURCE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_ORIGINATING_URI() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_ORIGINATING_URI", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_PACKAGE_NAME() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_PACKAGE_NAME", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_PERMISSION_GROUP_NAME() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_PERMISSION_GROUP_NAME", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_PHONE_NUMBER() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_PHONE_NUMBER", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_PROCESS_TEXT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_PROCESS_TEXT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_PROCESS_TEXT_READONLY() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_PROCESS_TEXT_READONLY", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_QUICK_VIEW_FEATURES() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_QUICK_VIEW_FEATURES", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_QUIET_MODE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_QUIET_MODE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_REFERRER() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_REFERRER", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_REFERRER_NAME() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_REFERRER_NAME", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_REMOTE_INTENT_TOKEN() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_REMOTE_INTENT_TOKEN", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_REPLACEMENT_EXTRAS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_REPLACEMENT_EXTRAS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_REPLACING() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_REPLACING", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_RESTRICTIONS_BUNDLE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_RESTRICTIONS_BUNDLE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_RESTRICTIONS_INTENT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_RESTRICTIONS_INTENT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_RESTRICTIONS_LIST() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_RESTRICTIONS_LIST", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_RESULT_RECEIVER() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_RESULT_RECEIVER", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_RETURN_RESULT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_RETURN_RESULT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_SHORTCUT_ICON() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_SHORTCUT_ICON", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_SHORTCUT_ICON_RESOURCE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_SHORTCUT_ICON_RESOURCE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_SHORTCUT_ID() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_SHORTCUT_ID", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_SHORTCUT_INTENT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_SHORTCUT_INTENT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_SHORTCUT_NAME() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_SHORTCUT_NAME", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_SHUTDOWN_USERSPACE_ONLY() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_SHUTDOWN_USERSPACE_ONLY", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_SPLIT_NAME() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_SPLIT_NAME", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_START_TIME() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_START_TIME", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_STREAM() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_STREAM", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_SUBJECT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_SUBJECT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_SUSPENDED_PACKAGE_EXTRAS() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_SUSPENDED_PACKAGE_EXTRAS", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_TEMPLATE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_TEMPLATE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_TEXT() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_TEXT", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_TIME() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_TIME", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_TIMEZONE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_TIMEZONE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_TITLE() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_TITLE", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_UID() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_UID", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_USER() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_USER", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) EXTRA_USER_INITIATED() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EXTRA_USER_INITIATED", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FILL_IN_ACTION() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FILL_IN_ACTION", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FILL_IN_CATEGORIES() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FILL_IN_CATEGORIES", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FILL_IN_CLIP_DATA() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FILL_IN_CLIP_DATA", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FILL_IN_COMPONENT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FILL_IN_COMPONENT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FILL_IN_DATA() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FILL_IN_DATA", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FILL_IN_IDENTIFIER() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FILL_IN_IDENTIFIER", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FILL_IN_PACKAGE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FILL_IN_PACKAGE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FILL_IN_SELECTOR() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FILL_IN_SELECTOR", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FILL_IN_SOURCE_BOUNDS() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FILL_IN_SOURCE_BOUNDS", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_ACTIVITY_BROUGHT_TO_FRONT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_ACTIVITY_BROUGHT_TO_FRONT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_ACTIVITY_CLEAR_TASK() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_ACTIVITY_CLEAR_TASK", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_ACTIVITY_CLEAR_TOP() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_ACTIVITY_CLEAR_TOP", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_ACTIVITY_FORWARD_RESULT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_ACTIVITY_FORWARD_RESULT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_ACTIVITY_LAUNCH_ADJACENT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_ACTIVITY_LAUNCH_ADJACENT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_ACTIVITY_MATCH_EXTERNAL() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_ACTIVITY_MATCH_EXTERNAL", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_ACTIVITY_MULTIPLE_TASK() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_ACTIVITY_MULTIPLE_TASK", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_ACTIVITY_NEW_DOCUMENT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_ACTIVITY_NEW_DOCUMENT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_ACTIVITY_NEW_TASK() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_ACTIVITY_NEW_TASK", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_ACTIVITY_NO_ANIMATION() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_ACTIVITY_NO_ANIMATION", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_ACTIVITY_NO_HISTORY() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_ACTIVITY_NO_HISTORY", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_ACTIVITY_NO_USER_ACTION() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_ACTIVITY_NO_USER_ACTION", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_ACTIVITY_PREVIOUS_IS_TOP() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_ACTIVITY_PREVIOUS_IS_TOP", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_ACTIVITY_REORDER_TO_FRONT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_ACTIVITY_REORDER_TO_FRONT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_ACTIVITY_REQUIRE_DEFAULT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_ACTIVITY_REQUIRE_DEFAULT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_ACTIVITY_REQUIRE_NON_BROWSER() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_ACTIVITY_REQUIRE_NON_BROWSER", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_ACTIVITY_RESET_TASK_IF_NEEDED() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_ACTIVITY_RESET_TASK_IF_NEEDED", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_ACTIVITY_RETAIN_IN_RECENTS() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_ACTIVITY_RETAIN_IN_RECENTS", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_ACTIVITY_SINGLE_TOP() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_ACTIVITY_SINGLE_TOP", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_ACTIVITY_TASK_ON_HOME() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_ACTIVITY_TASK_ON_HOME", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_DEBUG_LOG_RESOLUTION() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_DEBUG_LOG_RESOLUTION", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_DIRECT_BOOT_AUTO() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_DIRECT_BOOT_AUTO", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_EXCLUDE_STOPPED_PACKAGES() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_EXCLUDE_STOPPED_PACKAGES", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_FROM_BACKGROUND() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_FROM_BACKGROUND", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_GRANT_PERSISTABLE_URI_PERMISSION() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_GRANT_PERSISTABLE_URI_PERMISSION", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_GRANT_PREFIX_URI_PERMISSION() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_GRANT_PREFIX_URI_PERMISSION", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_GRANT_READ_URI_PERMISSION() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_GRANT_READ_URI_PERMISSION", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_GRANT_WRITE_URI_PERMISSION() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_GRANT_WRITE_URI_PERMISSION", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_INCLUDE_STOPPED_PACKAGES() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_INCLUDE_STOPPED_PACKAGES", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_RECEIVER_FOREGROUND() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_RECEIVER_FOREGROUND", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_RECEIVER_NO_ABORT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_RECEIVER_NO_ABORT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_RECEIVER_REGISTERED_ONLY() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_RECEIVER_REGISTERED_ONLY", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_RECEIVER_REPLACE_PENDING() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_RECEIVER_REPLACE_PENDING", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) FLAG_RECEIVER_VISIBLE_TO_INSTANT_APPS() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLAG_RECEIVER_VISIBLE_TO_INSTANT_APPS", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) METADATA_DOCK_HOME() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "METADATA_DOCK_HOME", "Ljava/lang/String;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) URI_ALLOW_UNSAFE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "URI_ALLOW_UNSAFE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) URI_ANDROID_APP_SCHEME() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "URI_ANDROID_APP_SCHEME", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentClass) URI_INTENT_SCHEME() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "URI_INTENT_SCHEME", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_IntentInstance) AddCategory(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "addCategory", "(Ljava/lang/String;)Landroid/content/Intent;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) AddFlags(arg0 int32) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "addFlags", "(I)Landroid/content/Intent;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((arg0)))
}

func (e *Android_content_IntentClass) Android_content_Intent(args ...interface{}) (*Android_content_IntentInstance, error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return nil, err
	}
	switch {
	case len(args) == 0:
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "()V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id)
		if err != nil {
			return nil, err
		}
		return Android_content_Intent_Instance(e.vmc, obj), nil

	case len(args) == 1 && strings.HasPrefix(types[0], "android/content/Intent"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "(Landroid/content/Intent;)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))))
		if err != nil {
			return nil, err
		}
		return Android_content_Intent_Instance(e.vmc, obj), nil

	case len(args) == 1 && strings.HasPrefix(types[0], "java/lang/String"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "(Ljava/lang/String;)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))))
		if err != nil {
			return nil, err
		}
		return Android_content_Intent_Instance(e.vmc, obj), nil

	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "android/net/Uri"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "(Ljava/lang/String;Landroid/net/Uri;)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
		if err != nil {
			return nil, err
		}
		return Android_content_Intent_Instance(e.vmc, obj), nil

	case len(args) == 2 && strings.HasPrefix(types[0], "android/content/Context") && strings.HasPrefix(types[1], "java/lang/Class"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "(Landroid/content/Context;Ljava/lang/Class;)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
		if err != nil {
			return nil, err
		}
		return Android_content_Intent_Instance(e.vmc, obj), nil

	case len(args) == 4 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "android/net/Uri") && strings.HasPrefix(types[2], "android/content/Context") && strings.HasPrefix(types[3], "java/lang/Class"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))), jni.Value(ObjVal(args[2].(jni.Object))), jni.Value(ObjVal(args[3].(jni.Object))))
		if err != nil {
			return nil, err
		}
		return Android_content_Intent_Instance(e.vmc, obj), nil
default:
		return nil, overloadError(types)
	}
}

func (e *Android_content_IntentInstance) Clone() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "clone", "()Ljava/lang/Object;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_IntentInstance) CloneFilter() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "cloneFilter", "()Landroid/content/Intent;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_IntentClass) CreateChooser(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 2 && strings.HasPrefix(types[0], "android/content/Intent") && strings.HasPrefix(types[1], "java/lang/CharSequence"):
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "createChooser", "(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 3 && strings.HasPrefix(types[0], "android/content/Intent") && strings.HasPrefix(types[1], "java/lang/CharSequence") && strings.HasPrefix(types[2], "android/content/IntentSender"):
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "createChooser", "(Landroid/content/Intent;Ljava/lang/CharSequence;Landroid/content/IntentSender;)Landroid/content/Intent;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))), jni.Value(ObjVal(args[2].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_IntentInstance) DescribeContents() (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "describeContents", "()I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_IntentInstance) FillIn(arg0 jni.Object, arg1 int32) (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "fillIn", "(Landroid/content/Intent;I)I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)))
}

func (e *Android_content_IntentInstance) FilterEquals(arg0 jni.Object) (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "filterEquals", "(Landroid/content/Intent;)Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) FilterHashCode() (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "filterHashCode", "()I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_IntentInstance) GetAction() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getAction", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_IntentInstance) GetBooleanArrayExtra(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getBooleanArrayExtra", "(Ljava/lang/String;)[Z")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) GetBooleanExtra(arg0 jni.Object, arg1 bool) (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getBooleanExtra", "(Ljava/lang/String;Z)Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(Boole(arg1)))
}

func (e *Android_content_IntentInstance) GetBundleExtra(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getBundleExtra", "(Ljava/lang/String;)Landroid/os/Bundle;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) GetByteArrayExtra(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getByteArrayExtra", "(Ljava/lang/String;)[B")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) GetByteExtra(arg0 jni.Object, arg1 byte) (ret byte, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getByteExtra", "(Ljava/lang/String;B)B")
	return jni.CallByteMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)))
}

func (e *Android_content_IntentInstance) GetCategories() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getCategories", "()Ljava/util/Set;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_IntentInstance) GetCharArrayExtra(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getCharArrayExtra", "(Ljava/lang/String;)[C")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) GetCharExtra(arg0 jni.Object, arg1 rune) (ret rune, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getCharExtra", "(Ljava/lang/String;C)C")
	return jni.CallCharMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)))
}

func (e *Android_content_IntentInstance) GetCharSequenceArrayExtra(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getCharSequenceArrayExtra", "(Ljava/lang/String;)[Ljava/lang/CharSequence;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) GetCharSequenceArrayListExtra(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getCharSequenceArrayListExtra", "(Ljava/lang/String;)Ljava/util/ArrayList;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) GetCharSequenceExtra(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getCharSequenceExtra", "(Ljava/lang/String;)Ljava/lang/CharSequence;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) GetClipData() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getClipData", "()Landroid/content/ClipData;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_IntentInstance) GetComponent() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getComponent", "()Landroid/content/ComponentName;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_IntentInstance) GetData() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getData", "()Landroid/net/Uri;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_IntentInstance) GetDataString() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getDataString", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_IntentInstance) GetDoubleArrayExtra(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getDoubleArrayExtra", "(Ljava/lang/String;)[D")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) GetDoubleExtra(arg0 jni.Object, arg1 float64) (ret float64, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getDoubleExtra", "(Ljava/lang/String;D)D")
	return jni.CallDoubleMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(math.Float64bits(arg1)))
}

func (e *Android_content_IntentInstance) GetExtras() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getExtras", "()Landroid/os/Bundle;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_IntentInstance) GetFlags() (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getFlags", "()I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_IntentInstance) GetFloatArrayExtra(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getFloatArrayExtra", "(Ljava/lang/String;)[F")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) GetFloatExtra(arg0 jni.Object, arg1 float32) (ret float32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getFloatExtra", "(Ljava/lang/String;F)F")
	return jni.CallFloatMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(math.Float32bits(arg1)))
}

func (e *Android_content_IntentInstance) GetIdentifier() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getIdentifier", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_IntentInstance) GetIntArrayExtra(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getIntArrayExtra", "(Ljava/lang/String;)[I")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) GetIntExtra(arg0 jni.Object, arg1 int32) (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getIntExtra", "(Ljava/lang/String;I)I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)))
}

func (e *Android_content_IntentInstance) GetIntegerArrayListExtra(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getIntegerArrayListExtra", "(Ljava/lang/String;)Ljava/util/ArrayList;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentClass) GetIntent(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "getIntent", "(Ljava/lang/String;)Landroid/content/Intent;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentClass) GetIntentOld(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "getIntentOld", "(Ljava/lang/String;)Landroid/content/Intent;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) GetLongArrayExtra(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getLongArrayExtra", "(Ljava/lang/String;)[J")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) GetLongExtra(arg0 jni.Object, arg1 int64) (ret int64, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getLongExtra", "(Ljava/lang/String;J)J")
	return jni.CallLongMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)))
}

func (e *Android_content_IntentInstance) GetPackage() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getPackage", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_IntentInstance) GetParcelableArrayExtra(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getParcelableArrayExtra", "(Ljava/lang/String;)[Landroid/os/Parcelable;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) GetParcelableArrayListExtra(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getParcelableArrayListExtra", "(Ljava/lang/String;)Ljava/util/ArrayList;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) GetParcelableExtra(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getParcelableExtra", "(Ljava/lang/String;)Landroid/os/Parcelable;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) GetScheme() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getScheme", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_IntentInstance) GetSelector() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getSelector", "()Landroid/content/Intent;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_IntentInstance) GetSerializableExtra(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getSerializableExtra", "(Ljava/lang/String;)Ljava/io/Serializable;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) GetShortArrayExtra(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getShortArrayExtra", "(Ljava/lang/String;)[S")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) GetShortExtra(arg0 jni.Object, arg1 int16) (ret int16, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getShortExtra", "(Ljava/lang/String;S)S")
	return jni.CallShortMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)))
}

func (e *Android_content_IntentInstance) GetSourceBounds() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getSourceBounds", "()Landroid/graphics/Rect;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_IntentInstance) GetStringArrayExtra(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getStringArrayExtra", "(Ljava/lang/String;)[Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) GetStringArrayListExtra(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getStringArrayListExtra", "(Ljava/lang/String;)Ljava/util/ArrayList;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) GetStringExtra(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getStringExtra", "(Ljava/lang/String;)Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) GetType() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getType", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_IntentInstance) HasCategory(arg0 jni.Object) (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "hasCategory", "(Ljava/lang/String;)Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) HasExtra(arg0 jni.Object) (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "hasExtra", "(Ljava/lang/String;)Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) HasFileDescriptors() (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "hasFileDescriptors", "()Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_IntentClass) MakeMainActivity(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "makeMainActivity", "(Landroid/content/ComponentName;)Landroid/content/Intent;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentClass) MakeMainSelectorActivity(arg0 jni.Object, arg1 jni.Object) (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "makeMainSelectorActivity", "(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)))
}

func (e *Android_content_IntentClass) MakeRestartActivityTask(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "makeRestartActivityTask", "(Landroid/content/ComponentName;)Landroid/content/Intent;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentClass) NormalizeMimeType(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "normalizeMimeType", "(Ljava/lang/String;)Ljava/lang/String;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentClass) ParseIntent(arg0 jni.Object, arg1 jni.Object, arg2 jni.Object) (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "parseIntent", "(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)Landroid/content/Intent;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)), jni.Value(ObjVal(arg2)))
}

func (e *Android_content_IntentClass) ParseUri(arg0 jni.Object, arg1 int32) (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "parseUri", "(Ljava/lang/String;I)Landroid/content/Intent;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)))
}

func (e *Android_content_IntentInstance) PutCharSequenceArrayListExtra(arg0 jni.Object, arg1 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putCharSequenceArrayListExtra", "(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)))
}

func (e *Android_content_IntentInstance) PutExtra(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && types[1] == "bool":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtra", "(Ljava/lang/String;Z)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(Boole(args[1].(bool))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && types[1] == "byte":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtra", "(Ljava/lang/String;B)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(byte))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && types[1] == "rune":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtra", "(Ljava/lang/String;C)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(rune))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && types[1] == "int16":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtra", "(Ljava/lang/String;S)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int16))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && types[1] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtra", "(Ljava/lang/String;I)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && types[1] == "int64":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtra", "(Ljava/lang/String;J)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int64))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && types[1] == "float32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtra", "(Ljava/lang/String;F)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(math.Float32bits(args[1].(float32))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && types[1] == "float64":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtra", "(Ljava/lang/String;D)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(math.Float64bits(args[1].(float64))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "java/lang/String"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtra", "(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "java/lang/CharSequence"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtra", "(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "android/os/Parcelable"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtra", "(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "[Landroid/os/Parcelable;"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtra", "(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "java/io/Serializable"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtra", "(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "[Z"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtra", "(Ljava/lang/String;[Z)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "[B"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtra", "(Ljava/lang/String;[B)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "[S"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtra", "(Ljava/lang/String;[S)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "[C"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtra", "(Ljava/lang/String;[C)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "[I"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtra", "(Ljava/lang/String;[I)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "[J"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtra", "(Ljava/lang/String;[J)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "[F"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtra", "(Ljava/lang/String;[F)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "[D"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtra", "(Ljava/lang/String;[D)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "[Ljava/lang/String;"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtra", "(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "[Ljava/lang/CharSequence;"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtra", "(Ljava/lang/String;[Ljava/lang/CharSequence;)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "android/os/Bundle"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtra", "(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_IntentInstance) PutExtras(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 1 && strings.HasPrefix(types[0], "android/content/Intent"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtras", "(Landroid/content/Intent;)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 1 && strings.HasPrefix(types[0], "android/os/Bundle"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putExtras", "(Landroid/os/Bundle;)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_IntentInstance) PutIntegerArrayListExtra(arg0 jni.Object, arg1 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putIntegerArrayListExtra", "(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)))
}

func (e *Android_content_IntentInstance) PutParcelableArrayListExtra(arg0 jni.Object, arg1 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putParcelableArrayListExtra", "(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)))
}

func (e *Android_content_IntentInstance) PutStringArrayListExtra(arg0 jni.Object, arg1 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "putStringArrayListExtra", "(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)))
}

func (e *Android_content_IntentInstance) ReadFromParcel(arg0 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "readFromParcel", "(Landroid/os/Parcel;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) RemoveCategory(arg0 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "removeCategory", "(Ljava/lang/String;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) RemoveExtra(arg0 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "removeExtra", "(Ljava/lang/String;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) RemoveFlags(arg0 int32) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "removeFlags", "(I)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((arg0)))
}

func (e *Android_content_IntentInstance) ReplaceExtras(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 1 && strings.HasPrefix(types[0], "android/content/Intent"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "replaceExtras", "(Landroid/content/Intent;)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 1 && strings.HasPrefix(types[0], "android/os/Bundle"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "replaceExtras", "(Landroid/os/Bundle;)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_IntentInstance) ResolveActivity(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "resolveActivity", "(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) ResolveActivityInfo(arg0 jni.Object, arg1 int32) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "resolveActivityInfo", "(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)))
}

func (e *Android_content_IntentInstance) ResolveType(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 1 && strings.HasPrefix(types[0], "android/content/Context"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "resolveType", "(Landroid/content/Context;)Ljava/lang/String;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 1 && strings.HasPrefix(types[0], "android/content/ContentResolver"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "resolveType", "(Landroid/content/ContentResolver;)Ljava/lang/String;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_IntentInstance) ResolveTypeIfNeeded(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "resolveTypeIfNeeded", "(Landroid/content/ContentResolver;)Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) SetAction(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setAction", "(Ljava/lang/String;)Landroid/content/Intent;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) SetClass(arg0 jni.Object, arg1 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setClass", "(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)))
}

func (e *Android_content_IntentInstance) SetClassName(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 2 && strings.HasPrefix(types[0], "android/content/Context") && strings.HasPrefix(types[1], "java/lang/String"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setClassName", "(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "java/lang/String"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setClassName", "(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_IntentInstance) SetClipData(arg0 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setClipData", "(Landroid/content/ClipData;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) SetComponent(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setComponent", "(Landroid/content/ComponentName;)Landroid/content/Intent;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) SetData(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setData", "(Landroid/net/Uri;)Landroid/content/Intent;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) SetDataAndNormalize(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setDataAndNormalize", "(Landroid/net/Uri;)Landroid/content/Intent;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) SetDataAndType(arg0 jni.Object, arg1 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setDataAndType", "(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)))
}

func (e *Android_content_IntentInstance) SetDataAndTypeAndNormalize(arg0 jni.Object, arg1 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setDataAndTypeAndNormalize", "(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)))
}

func (e *Android_content_IntentInstance) SetExtrasClassLoader(arg0 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setExtrasClassLoader", "(Ljava/lang/ClassLoader;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) SetFlags(arg0 int32) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setFlags", "(I)Landroid/content/Intent;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((arg0)))
}

func (e *Android_content_IntentInstance) SetIdentifier(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setIdentifier", "(Ljava/lang/String;)Landroid/content/Intent;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) SetPackage(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setPackage", "(Ljava/lang/String;)Landroid/content/Intent;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) SetSelector(arg0 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setSelector", "(Landroid/content/Intent;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) SetSourceBounds(arg0 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setSourceBounds", "(Landroid/graphics/Rect;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) SetType(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setType", "(Ljava/lang/String;)Landroid/content/Intent;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) SetTypeAndNormalize(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setTypeAndNormalize", "(Ljava/lang/String;)Landroid/content/Intent;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_IntentInstance) ToString() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "toString", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_IntentInstance) ToURI() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "toURI", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_IntentInstance) ToUri(arg0 int32) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "toUri", "(I)Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((arg0)))
}

func (e *Android_content_IntentInstance) WriteToParcel(arg0 jni.Object, arg1 int32) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "writeToParcel", "(Landroid/os/Parcel;I)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)))
}

