package androidlib

import (
	"math"
	"strings"

	"git.wow.st/gmp/jni"
)

var _ = math.Float32bits
var _ = strings.HasPrefix

type Android_hardware_camera2_CameraMetadataClass struct {
	vmc   *VMContext
	Class jni.Class
}

type Android_hardware_camera2_CameraMetadataInstance struct {
	*Android_hardware_camera2_CameraMetadataClass
	Jobj jni.Object
}

func Android_hardware_camera2_CameraMetadata(vmc *VMContext) *Android_hardware_camera2_CameraMetadataClass {
	return &Android_hardware_camera2_CameraMetadataClass{
		vmc:   vmc,
		Class: jni.FindClass(vmc.JNIEnv, "android/hardware/camera2/CameraMetadata"),
	}
}

func Android_hardware_camera2_CameraMetadata_Instance(vmc *VMContext, jobj jni.Object) *Android_hardware_camera2_CameraMetadataInstance {
	return &Android_hardware_camera2_CameraMetadataInstance{
		Android_hardware_camera2_CameraMetadataClass: Android_hardware_camera2_CameraMetadata(vmc),
		Jobj: jobj,
	}
}

func (e *Android_hardware_camera2_CameraMetadataClass) COLOR_CORRECTION_ABERRATION_MODE_FAST() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "COLOR_CORRECTION_ABERRATION_MODE_FAST", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) COLOR_CORRECTION_ABERRATION_MODE_HIGH_QUALITY() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "COLOR_CORRECTION_ABERRATION_MODE_HIGH_QUALITY", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) COLOR_CORRECTION_ABERRATION_MODE_OFF() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "COLOR_CORRECTION_ABERRATION_MODE_OFF", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) COLOR_CORRECTION_MODE_FAST() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "COLOR_CORRECTION_MODE_FAST", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) COLOR_CORRECTION_MODE_HIGH_QUALITY() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "COLOR_CORRECTION_MODE_HIGH_QUALITY", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) COLOR_CORRECTION_MODE_TRANSFORM_MATRIX() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "COLOR_CORRECTION_MODE_TRANSFORM_MATRIX", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AE_ANTIBANDING_MODE_50HZ() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_ANTIBANDING_MODE_50HZ", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AE_ANTIBANDING_MODE_60HZ() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_ANTIBANDING_MODE_60HZ", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AE_ANTIBANDING_MODE_AUTO() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_ANTIBANDING_MODE_AUTO", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AE_ANTIBANDING_MODE_OFF() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_ANTIBANDING_MODE_OFF", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AE_MODE_OFF() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_MODE_OFF", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AE_MODE_ON() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_MODE_ON", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AE_MODE_ON_ALWAYS_FLASH() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_MODE_ON_ALWAYS_FLASH", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AE_MODE_ON_AUTO_FLASH() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_MODE_ON_AUTO_FLASH", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AE_MODE_ON_AUTO_FLASH_REDEYE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_MODE_ON_AUTO_FLASH_REDEYE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AE_MODE_ON_EXTERNAL_FLASH() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_MODE_ON_EXTERNAL_FLASH", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AE_PRECAPTURE_TRIGGER_CANCEL() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_PRECAPTURE_TRIGGER_CANCEL", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AE_PRECAPTURE_TRIGGER_IDLE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_PRECAPTURE_TRIGGER_IDLE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AE_PRECAPTURE_TRIGGER_START() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_PRECAPTURE_TRIGGER_START", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AE_STATE_CONVERGED() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_STATE_CONVERGED", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AE_STATE_FLASH_REQUIRED() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_STATE_FLASH_REQUIRED", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AE_STATE_INACTIVE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_STATE_INACTIVE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AE_STATE_LOCKED() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_STATE_LOCKED", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AE_STATE_PRECAPTURE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_STATE_PRECAPTURE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AE_STATE_SEARCHING() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AE_STATE_SEARCHING", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AF_MODE_AUTO() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AF_MODE_AUTO", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AF_MODE_CONTINUOUS_PICTURE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AF_MODE_CONTINUOUS_PICTURE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AF_MODE_CONTINUOUS_VIDEO() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AF_MODE_CONTINUOUS_VIDEO", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AF_MODE_EDOF() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AF_MODE_EDOF", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AF_MODE_MACRO() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AF_MODE_MACRO", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AF_MODE_OFF() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AF_MODE_OFF", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AF_SCENE_CHANGE_DETECTED() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AF_SCENE_CHANGE_DETECTED", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AF_SCENE_CHANGE_NOT_DETECTED() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AF_SCENE_CHANGE_NOT_DETECTED", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AF_STATE_ACTIVE_SCAN() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AF_STATE_ACTIVE_SCAN", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AF_STATE_FOCUSED_LOCKED() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AF_STATE_FOCUSED_LOCKED", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AF_STATE_INACTIVE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AF_STATE_INACTIVE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AF_STATE_NOT_FOCUSED_LOCKED() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AF_STATE_NOT_FOCUSED_LOCKED", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AF_STATE_PASSIVE_FOCUSED() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AF_STATE_PASSIVE_FOCUSED", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AF_STATE_PASSIVE_SCAN() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AF_STATE_PASSIVE_SCAN", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AF_STATE_PASSIVE_UNFOCUSED() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AF_STATE_PASSIVE_UNFOCUSED", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AF_TRIGGER_CANCEL() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AF_TRIGGER_CANCEL", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AF_TRIGGER_IDLE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AF_TRIGGER_IDLE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AF_TRIGGER_START() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AF_TRIGGER_START", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AWB_MODE_AUTO() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AWB_MODE_AUTO", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AWB_MODE_CLOUDY_DAYLIGHT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AWB_MODE_CLOUDY_DAYLIGHT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AWB_MODE_DAYLIGHT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AWB_MODE_DAYLIGHT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AWB_MODE_FLUORESCENT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AWB_MODE_FLUORESCENT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AWB_MODE_INCANDESCENT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AWB_MODE_INCANDESCENT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AWB_MODE_OFF() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AWB_MODE_OFF", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AWB_MODE_SHADE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AWB_MODE_SHADE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AWB_MODE_TWILIGHT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AWB_MODE_TWILIGHT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AWB_MODE_WARM_FLUORESCENT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AWB_MODE_WARM_FLUORESCENT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AWB_STATE_CONVERGED() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AWB_STATE_CONVERGED", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AWB_STATE_INACTIVE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AWB_STATE_INACTIVE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AWB_STATE_LOCKED() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AWB_STATE_LOCKED", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_AWB_STATE_SEARCHING() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_AWB_STATE_SEARCHING", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_CAPTURE_INTENT_CUSTOM() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_CAPTURE_INTENT_CUSTOM", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_CAPTURE_INTENT_MANUAL() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_CAPTURE_INTENT_MANUAL", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_CAPTURE_INTENT_MOTION_TRACKING() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_CAPTURE_INTENT_MOTION_TRACKING", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_CAPTURE_INTENT_PREVIEW() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_CAPTURE_INTENT_PREVIEW", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_CAPTURE_INTENT_STILL_CAPTURE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_CAPTURE_INTENT_STILL_CAPTURE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_CAPTURE_INTENT_VIDEO_RECORD() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_CAPTURE_INTENT_VIDEO_RECORD", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_CAPTURE_INTENT_VIDEO_SNAPSHOT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_CAPTURE_INTENT_VIDEO_SNAPSHOT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_CAPTURE_INTENT_ZERO_SHUTTER_LAG() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_CAPTURE_INTENT_ZERO_SHUTTER_LAG", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_EFFECT_MODE_AQUA() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_EFFECT_MODE_AQUA", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_EFFECT_MODE_BLACKBOARD() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_EFFECT_MODE_BLACKBOARD", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_EFFECT_MODE_MONO() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_EFFECT_MODE_MONO", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_EFFECT_MODE_NEGATIVE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_EFFECT_MODE_NEGATIVE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_EFFECT_MODE_OFF() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_EFFECT_MODE_OFF", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_EFFECT_MODE_POSTERIZE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_EFFECT_MODE_POSTERIZE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_EFFECT_MODE_SEPIA() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_EFFECT_MODE_SEPIA", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_EFFECT_MODE_SOLARIZE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_EFFECT_MODE_SOLARIZE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_EFFECT_MODE_WHITEBOARD() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_EFFECT_MODE_WHITEBOARD", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_EXTENDED_SCENE_MODE_BOKEH_CONTINUOUS() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_EXTENDED_SCENE_MODE_BOKEH_CONTINUOUS", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_EXTENDED_SCENE_MODE_BOKEH_STILL_CAPTURE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_EXTENDED_SCENE_MODE_BOKEH_STILL_CAPTURE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_EXTENDED_SCENE_MODE_DISABLED() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_EXTENDED_SCENE_MODE_DISABLED", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_MODE_AUTO() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_MODE_AUTO", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_MODE_OFF() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_MODE_OFF", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_MODE_OFF_KEEP_STATE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_MODE_OFF_KEEP_STATE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_MODE_USE_EXTENDED_SCENE_MODE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_MODE_USE_EXTENDED_SCENE_MODE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_MODE_USE_SCENE_MODE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_MODE_USE_SCENE_MODE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_SCENE_MODE_ACTION() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_SCENE_MODE_ACTION", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_SCENE_MODE_BARCODE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_SCENE_MODE_BARCODE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_SCENE_MODE_BEACH() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_SCENE_MODE_BEACH", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_SCENE_MODE_CANDLELIGHT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_SCENE_MODE_CANDLELIGHT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_SCENE_MODE_DISABLED() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_SCENE_MODE_DISABLED", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_SCENE_MODE_FACE_PRIORITY() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_SCENE_MODE_FACE_PRIORITY", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_SCENE_MODE_FIREWORKS() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_SCENE_MODE_FIREWORKS", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_SCENE_MODE_HDR() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_SCENE_MODE_HDR", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_SCENE_MODE_HIGH_SPEED_VIDEO() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_SCENE_MODE_HIGH_SPEED_VIDEO", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_SCENE_MODE_LANDSCAPE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_SCENE_MODE_LANDSCAPE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_SCENE_MODE_NIGHT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_SCENE_MODE_NIGHT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_SCENE_MODE_NIGHT_PORTRAIT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_SCENE_MODE_NIGHT_PORTRAIT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_SCENE_MODE_PARTY() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_SCENE_MODE_PARTY", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_SCENE_MODE_PORTRAIT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_SCENE_MODE_PORTRAIT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_SCENE_MODE_SNOW() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_SCENE_MODE_SNOW", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_SCENE_MODE_SPORTS() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_SCENE_MODE_SPORTS", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_SCENE_MODE_STEADYPHOTO() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_SCENE_MODE_STEADYPHOTO", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_SCENE_MODE_SUNSET() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_SCENE_MODE_SUNSET", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_SCENE_MODE_THEATRE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_SCENE_MODE_THEATRE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_VIDEO_STABILIZATION_MODE_OFF() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_VIDEO_STABILIZATION_MODE_OFF", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) CONTROL_VIDEO_STABILIZATION_MODE_ON() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CONTROL_VIDEO_STABILIZATION_MODE_ON", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) DISTORTION_CORRECTION_MODE_FAST() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DISTORTION_CORRECTION_MODE_FAST", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) DISTORTION_CORRECTION_MODE_HIGH_QUALITY() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DISTORTION_CORRECTION_MODE_HIGH_QUALITY", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) DISTORTION_CORRECTION_MODE_OFF() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "DISTORTION_CORRECTION_MODE_OFF", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) EDGE_MODE_FAST() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EDGE_MODE_FAST", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) EDGE_MODE_HIGH_QUALITY() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EDGE_MODE_HIGH_QUALITY", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) EDGE_MODE_OFF() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EDGE_MODE_OFF", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) EDGE_MODE_ZERO_SHUTTER_LAG() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "EDGE_MODE_ZERO_SHUTTER_LAG", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) FLASH_MODE_OFF() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLASH_MODE_OFF", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) FLASH_MODE_SINGLE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLASH_MODE_SINGLE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) FLASH_MODE_TORCH() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLASH_MODE_TORCH", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) FLASH_STATE_CHARGING() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLASH_STATE_CHARGING", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) FLASH_STATE_FIRED() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLASH_STATE_FIRED", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) FLASH_STATE_PARTIAL() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLASH_STATE_PARTIAL", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) FLASH_STATE_READY() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLASH_STATE_READY", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) FLASH_STATE_UNAVAILABLE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "FLASH_STATE_UNAVAILABLE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) HOT_PIXEL_MODE_FAST() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "HOT_PIXEL_MODE_FAST", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) HOT_PIXEL_MODE_HIGH_QUALITY() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "HOT_PIXEL_MODE_HIGH_QUALITY", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) HOT_PIXEL_MODE_OFF() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "HOT_PIXEL_MODE_OFF", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) INFO_SUPPORTED_HARDWARE_LEVEL_3() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "INFO_SUPPORTED_HARDWARE_LEVEL_3", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) INFO_SUPPORTED_HARDWARE_LEVEL_EXTERNAL() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "INFO_SUPPORTED_HARDWARE_LEVEL_EXTERNAL", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) INFO_SUPPORTED_HARDWARE_LEVEL_FULL() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "INFO_SUPPORTED_HARDWARE_LEVEL_FULL", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) INFO_SUPPORTED_HARDWARE_LEVEL_LEGACY() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "INFO_SUPPORTED_HARDWARE_LEVEL_LEGACY", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) INFO_SUPPORTED_HARDWARE_LEVEL_LIMITED() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "INFO_SUPPORTED_HARDWARE_LEVEL_LIMITED", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) LENS_FACING_BACK() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_FACING_BACK", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) LENS_FACING_EXTERNAL() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_FACING_EXTERNAL", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) LENS_FACING_FRONT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_FACING_FRONT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) LENS_INFO_FOCUS_DISTANCE_CALIBRATION_APPROXIMATE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_INFO_FOCUS_DISTANCE_CALIBRATION_APPROXIMATE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) LENS_INFO_FOCUS_DISTANCE_CALIBRATION_CALIBRATED() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_INFO_FOCUS_DISTANCE_CALIBRATION_CALIBRATED", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) LENS_INFO_FOCUS_DISTANCE_CALIBRATION_UNCALIBRATED() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_INFO_FOCUS_DISTANCE_CALIBRATION_UNCALIBRATED", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) LENS_OPTICAL_STABILIZATION_MODE_OFF() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_OPTICAL_STABILIZATION_MODE_OFF", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) LENS_OPTICAL_STABILIZATION_MODE_ON() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_OPTICAL_STABILIZATION_MODE_ON", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) LENS_POSE_REFERENCE_GYROSCOPE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_POSE_REFERENCE_GYROSCOPE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) LENS_POSE_REFERENCE_PRIMARY_CAMERA() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_POSE_REFERENCE_PRIMARY_CAMERA", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) LENS_POSE_REFERENCE_UNDEFINED() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_POSE_REFERENCE_UNDEFINED", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) LENS_STATE_MOVING() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_STATE_MOVING", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) LENS_STATE_STATIONARY() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LENS_STATE_STATIONARY", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) LOGICAL_MULTI_CAMERA_SENSOR_SYNC_TYPE_APPROXIMATE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LOGICAL_MULTI_CAMERA_SENSOR_SYNC_TYPE_APPROXIMATE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) LOGICAL_MULTI_CAMERA_SENSOR_SYNC_TYPE_CALIBRATED() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "LOGICAL_MULTI_CAMERA_SENSOR_SYNC_TYPE_CALIBRATED", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) NOISE_REDUCTION_MODE_FAST() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "NOISE_REDUCTION_MODE_FAST", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) NOISE_REDUCTION_MODE_HIGH_QUALITY() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "NOISE_REDUCTION_MODE_HIGH_QUALITY", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) NOISE_REDUCTION_MODE_MINIMAL() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "NOISE_REDUCTION_MODE_MINIMAL", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) NOISE_REDUCTION_MODE_OFF() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "NOISE_REDUCTION_MODE_OFF", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) NOISE_REDUCTION_MODE_ZERO_SHUTTER_LAG() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "NOISE_REDUCTION_MODE_ZERO_SHUTTER_LAG", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) REQUEST_AVAILABLE_CAPABILITIES_BACKWARD_COMPATIBLE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_AVAILABLE_CAPABILITIES_BACKWARD_COMPATIBLE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) REQUEST_AVAILABLE_CAPABILITIES_BURST_CAPTURE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_AVAILABLE_CAPABILITIES_BURST_CAPTURE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) REQUEST_AVAILABLE_CAPABILITIES_CONSTRAINED_HIGH_SPEED_VIDEO() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_AVAILABLE_CAPABILITIES_CONSTRAINED_HIGH_SPEED_VIDEO", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) REQUEST_AVAILABLE_CAPABILITIES_DEPTH_OUTPUT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_AVAILABLE_CAPABILITIES_DEPTH_OUTPUT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) REQUEST_AVAILABLE_CAPABILITIES_LOGICAL_MULTI_CAMERA() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_AVAILABLE_CAPABILITIES_LOGICAL_MULTI_CAMERA", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) REQUEST_AVAILABLE_CAPABILITIES_MANUAL_POST_PROCESSING() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_AVAILABLE_CAPABILITIES_MANUAL_POST_PROCESSING", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) REQUEST_AVAILABLE_CAPABILITIES_MANUAL_SENSOR() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_AVAILABLE_CAPABILITIES_MANUAL_SENSOR", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) REQUEST_AVAILABLE_CAPABILITIES_MONOCHROME() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_AVAILABLE_CAPABILITIES_MONOCHROME", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) REQUEST_AVAILABLE_CAPABILITIES_MOTION_TRACKING() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_AVAILABLE_CAPABILITIES_MOTION_TRACKING", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) REQUEST_AVAILABLE_CAPABILITIES_OFFLINE_PROCESSING() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_AVAILABLE_CAPABILITIES_OFFLINE_PROCESSING", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) REQUEST_AVAILABLE_CAPABILITIES_PRIVATE_REPROCESSING() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_AVAILABLE_CAPABILITIES_PRIVATE_REPROCESSING", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) REQUEST_AVAILABLE_CAPABILITIES_RAW() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_AVAILABLE_CAPABILITIES_RAW", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) REQUEST_AVAILABLE_CAPABILITIES_READ_SENSOR_SETTINGS() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_AVAILABLE_CAPABILITIES_READ_SENSOR_SETTINGS", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) REQUEST_AVAILABLE_CAPABILITIES_REMOSAIC_REPROCESSING() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_AVAILABLE_CAPABILITIES_REMOSAIC_REPROCESSING", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) REQUEST_AVAILABLE_CAPABILITIES_SECURE_IMAGE_DATA() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_AVAILABLE_CAPABILITIES_SECURE_IMAGE_DATA", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) REQUEST_AVAILABLE_CAPABILITIES_SYSTEM_CAMERA() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_AVAILABLE_CAPABILITIES_SYSTEM_CAMERA", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) REQUEST_AVAILABLE_CAPABILITIES_ULTRA_HIGH_RESOLUTION_SENSOR() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_AVAILABLE_CAPABILITIES_ULTRA_HIGH_RESOLUTION_SENSOR", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) REQUEST_AVAILABLE_CAPABILITIES_YUV_REPROCESSING() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "REQUEST_AVAILABLE_CAPABILITIES_YUV_REPROCESSING", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SCALER_CROPPING_TYPE_CENTER_ONLY() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SCALER_CROPPING_TYPE_CENTER_ONLY", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SCALER_CROPPING_TYPE_FREEFORM() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SCALER_CROPPING_TYPE_FREEFORM", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SCALER_ROTATE_AND_CROP_180() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SCALER_ROTATE_AND_CROP_180", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SCALER_ROTATE_AND_CROP_270() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SCALER_ROTATE_AND_CROP_270", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SCALER_ROTATE_AND_CROP_90() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SCALER_ROTATE_AND_CROP_90", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SCALER_ROTATE_AND_CROP_AUTO() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SCALER_ROTATE_AND_CROP_AUTO", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SCALER_ROTATE_AND_CROP_NONE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SCALER_ROTATE_AND_CROP_NONE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_BGGR() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_BGGR", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_GBRG() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_GBRG", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_GRBG() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_GRBG", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_MONO() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_MONO", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_NIR() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_NIR", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_RGB() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_RGB", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_RGGB() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_RGGB", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_INFO_TIMESTAMP_SOURCE_REALTIME() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_INFO_TIMESTAMP_SOURCE_REALTIME", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_INFO_TIMESTAMP_SOURCE_UNKNOWN() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_INFO_TIMESTAMP_SOURCE_UNKNOWN", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_PIXEL_MODE_DEFAULT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_PIXEL_MODE_DEFAULT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_PIXEL_MODE_MAXIMUM_RESOLUTION() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_PIXEL_MODE_MAXIMUM_RESOLUTION", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_REFERENCE_ILLUMINANT1_CLOUDY_WEATHER() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_REFERENCE_ILLUMINANT1_CLOUDY_WEATHER", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_REFERENCE_ILLUMINANT1_COOL_WHITE_FLUORESCENT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_REFERENCE_ILLUMINANT1_COOL_WHITE_FLUORESCENT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_REFERENCE_ILLUMINANT1_D50() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_REFERENCE_ILLUMINANT1_D50", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_REFERENCE_ILLUMINANT1_D55() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_REFERENCE_ILLUMINANT1_D55", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_REFERENCE_ILLUMINANT1_D65() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_REFERENCE_ILLUMINANT1_D65", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_REFERENCE_ILLUMINANT1_D75() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_REFERENCE_ILLUMINANT1_D75", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_REFERENCE_ILLUMINANT1_DAYLIGHT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_REFERENCE_ILLUMINANT1_DAYLIGHT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_REFERENCE_ILLUMINANT1_DAYLIGHT_FLUORESCENT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_REFERENCE_ILLUMINANT1_DAYLIGHT_FLUORESCENT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_REFERENCE_ILLUMINANT1_DAY_WHITE_FLUORESCENT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_REFERENCE_ILLUMINANT1_DAY_WHITE_FLUORESCENT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_REFERENCE_ILLUMINANT1_FINE_WEATHER() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_REFERENCE_ILLUMINANT1_FINE_WEATHER", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_REFERENCE_ILLUMINANT1_FLASH() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_REFERENCE_ILLUMINANT1_FLASH", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_REFERENCE_ILLUMINANT1_FLUORESCENT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_REFERENCE_ILLUMINANT1_FLUORESCENT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_REFERENCE_ILLUMINANT1_ISO_STUDIO_TUNGSTEN() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_REFERENCE_ILLUMINANT1_ISO_STUDIO_TUNGSTEN", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_REFERENCE_ILLUMINANT1_SHADE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_REFERENCE_ILLUMINANT1_SHADE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_REFERENCE_ILLUMINANT1_STANDARD_A() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_REFERENCE_ILLUMINANT1_STANDARD_A", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_REFERENCE_ILLUMINANT1_STANDARD_B() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_REFERENCE_ILLUMINANT1_STANDARD_B", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_REFERENCE_ILLUMINANT1_STANDARD_C() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_REFERENCE_ILLUMINANT1_STANDARD_C", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_REFERENCE_ILLUMINANT1_TUNGSTEN() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_REFERENCE_ILLUMINANT1_TUNGSTEN", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_REFERENCE_ILLUMINANT1_WHITE_FLUORESCENT() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_REFERENCE_ILLUMINANT1_WHITE_FLUORESCENT", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_TEST_PATTERN_MODE_COLOR_BARS() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_TEST_PATTERN_MODE_COLOR_BARS", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_TEST_PATTERN_MODE_COLOR_BARS_FADE_TO_GRAY() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_TEST_PATTERN_MODE_COLOR_BARS_FADE_TO_GRAY", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_TEST_PATTERN_MODE_CUSTOM1() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_TEST_PATTERN_MODE_CUSTOM1", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_TEST_PATTERN_MODE_OFF() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_TEST_PATTERN_MODE_OFF", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_TEST_PATTERN_MODE_PN9() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_TEST_PATTERN_MODE_PN9", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SENSOR_TEST_PATTERN_MODE_SOLID_COLOR() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SENSOR_TEST_PATTERN_MODE_SOLID_COLOR", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SHADING_MODE_FAST() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SHADING_MODE_FAST", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SHADING_MODE_HIGH_QUALITY() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SHADING_MODE_HIGH_QUALITY", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SHADING_MODE_OFF() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SHADING_MODE_OFF", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) STATISTICS_FACE_DETECT_MODE_FULL() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "STATISTICS_FACE_DETECT_MODE_FULL", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) STATISTICS_FACE_DETECT_MODE_OFF() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "STATISTICS_FACE_DETECT_MODE_OFF", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) STATISTICS_FACE_DETECT_MODE_SIMPLE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "STATISTICS_FACE_DETECT_MODE_SIMPLE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) STATISTICS_LENS_SHADING_MAP_MODE_OFF() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "STATISTICS_LENS_SHADING_MAP_MODE_OFF", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) STATISTICS_LENS_SHADING_MAP_MODE_ON() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "STATISTICS_LENS_SHADING_MAP_MODE_ON", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) STATISTICS_OIS_DATA_MODE_OFF() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "STATISTICS_OIS_DATA_MODE_OFF", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) STATISTICS_OIS_DATA_MODE_ON() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "STATISTICS_OIS_DATA_MODE_ON", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) STATISTICS_SCENE_FLICKER_50HZ() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "STATISTICS_SCENE_FLICKER_50HZ", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) STATISTICS_SCENE_FLICKER_60HZ() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "STATISTICS_SCENE_FLICKER_60HZ", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) STATISTICS_SCENE_FLICKER_NONE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "STATISTICS_SCENE_FLICKER_NONE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SYNC_MAX_LATENCY_PER_FRAME_CONTROL() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SYNC_MAX_LATENCY_PER_FRAME_CONTROL", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) SYNC_MAX_LATENCY_UNKNOWN() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "SYNC_MAX_LATENCY_UNKNOWN", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) TONEMAP_MODE_CONTRAST_CURVE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "TONEMAP_MODE_CONTRAST_CURVE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) TONEMAP_MODE_FAST() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "TONEMAP_MODE_FAST", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) TONEMAP_MODE_GAMMA_VALUE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "TONEMAP_MODE_GAMMA_VALUE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) TONEMAP_MODE_HIGH_QUALITY() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "TONEMAP_MODE_HIGH_QUALITY", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) TONEMAP_MODE_PRESET_CURVE() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "TONEMAP_MODE_PRESET_CURVE", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) TONEMAP_PRESET_CURVE_REC709() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "TONEMAP_PRESET_CURVE_REC709", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataClass) TONEMAP_PRESET_CURVE_SRGB() int32 {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "TONEMAP_PRESET_CURVE_SRGB", "I")
	return jni.GetStaticIntField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_hardware_camera2_CameraMetadataInstance) GetKeys() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getKeys", "()Ljava/util/List;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

