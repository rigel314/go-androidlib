package androidlib

import (
	"math"
	"strings"

	"git.wow.st/gmp/jni"
)

var _ = math.Float32bits
var _ = strings.HasPrefix

type Java_lang_StringClass struct {
	vmc   *VMContext
	Class jni.Class
}

type Java_lang_StringInstance struct {
	*Java_lang_StringClass
	Jobj jni.Object
}

func Java_lang_String(vmc *VMContext) *Java_lang_StringClass {
	return &Java_lang_StringClass{
		vmc:   vmc,
		Class: jni.FindClass(vmc.JNIEnv, "java/lang/String"),
	}
}

func Java_lang_String_Instance(vmc *VMContext, jobj jni.Object) *Java_lang_StringInstance {
	return &Java_lang_StringInstance{
		Java_lang_StringClass: Java_lang_String(vmc),
		Jobj: jobj,
	}
}

func (e *Java_lang_StringClass) CASE_INSENSITIVE_ORDER() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CASE_INSENSITIVE_ORDER", "Ljava/util/Comparator;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Java_lang_StringInstance) CharAt(arg0 int32) (ret rune, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "charAt", "(I)C")
	return jni.CallCharMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((arg0)))
}

func (e *Java_lang_StringInstance) CodePointAt(arg0 int32) (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "codePointAt", "(I)I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((arg0)))
}

func (e *Java_lang_StringInstance) CodePointBefore(arg0 int32) (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "codePointBefore", "(I)I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((arg0)))
}

func (e *Java_lang_StringInstance) CodePointCount(arg0 int32, arg1 int32) (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "codePointCount", "(II)I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((arg0)), jni.Value((arg1)))
}

func (e *Java_lang_StringInstance) CompareTo(args ...interface{}) (ret int32, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 1 && strings.HasPrefix(types[0], "java/lang/String"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "compareTo", "(Ljava/lang/String;)I")
		return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 1 && strings.HasPrefix(types[0], "java/lang/Object"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "compareTo", "(Ljava/lang/Object;)I")
		return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Java_lang_StringInstance) CompareToIgnoreCase(arg0 jni.Object) (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "compareToIgnoreCase", "(Ljava/lang/String;)I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Java_lang_StringInstance) Concat(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "concat", "(Ljava/lang/String;)Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Java_lang_StringInstance) Contains(arg0 jni.Object) (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "contains", "(Ljava/lang/CharSequence;)Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Java_lang_StringInstance) ContentEquals(args ...interface{}) (ret bool, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 1 && strings.HasPrefix(types[0], "java/lang/StringBuffer"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "contentEquals", "(Ljava/lang/StringBuffer;)Z")
		return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 1 && strings.HasPrefix(types[0], "java/lang/CharSequence"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "contentEquals", "(Ljava/lang/CharSequence;)Z")
		return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Java_lang_StringClass) CopyValueOf(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 3 && strings.HasPrefix(types[0], "[C") && types[1] == "int32" && types[2] == "int32":
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "copyValueOf", "([CII)Ljava/lang/String;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))), jni.Value((args[2].(int32))))
	case len(args) == 1 && strings.HasPrefix(types[0], "[C"):
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "copyValueOf", "([C)Ljava/lang/String;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Java_lang_StringInstance) EndsWith(arg0 jni.Object) (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "endsWith", "(Ljava/lang/String;)Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Java_lang_StringInstance) Equals(arg0 jni.Object) (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "equals", "(Ljava/lang/Object;)Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Java_lang_StringInstance) EqualsIgnoreCase(arg0 jni.Object) (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "equalsIgnoreCase", "(Ljava/lang/String;)Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Java_lang_StringClass) Format(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "[Ljava/lang/Object;"):
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "format", "(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 3 && strings.HasPrefix(types[0], "java/util/Locale") && strings.HasPrefix(types[1], "java/lang/String") && strings.HasPrefix(types[2], "[Ljava/lang/Object;"):
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "format", "(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))), jni.Value(ObjVal(args[2].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Java_lang_StringInstance) GetBytes(args ...interface{}) (ret interface{}, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 4 && types[0] == "int32" && types[1] == "int32" && strings.HasPrefix(types[2], "[B") && types[3] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getBytes", "(II[BI)V")
		err = jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((args[0].(int32))), jni.Value((args[1].(int32))), jni.Value(ObjVal(args[2].(jni.Object))), jni.Value((args[3].(int32))))
		return nil, err
	case len(args) == 1 && strings.HasPrefix(types[0], "java/lang/String"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getBytes", "(Ljava/lang/String;)[B")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 1 && strings.HasPrefix(types[0], "java/nio/charset/Charset"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getBytes", "(Ljava/nio/charset/Charset;)[B")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 0:
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getBytes", "()[B")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
	default:
		err = overloadError(types)
		return
	}
}

func (e *Java_lang_StringInstance) GetChars(arg0 int32, arg1 int32, arg2 jni.Object, arg3 int32) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getChars", "(II[CI)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((arg0)), jni.Value((arg1)), jni.Value(ObjVal(arg2)), jni.Value((arg3)))
}

func (e *Java_lang_StringInstance) HashCode() (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "hashCode", "()I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Java_lang_StringInstance) IndexOf(args ...interface{}) (ret int32, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 1 && types[0] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "indexOf", "(I)I")
		return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((args[0].(int32))))
	case len(args) == 2 && types[0] == "int32" && types[1] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "indexOf", "(II)I")
		return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((args[0].(int32))), jni.Value((args[1].(int32))))
	case len(args) == 1 && strings.HasPrefix(types[0], "java/lang/String"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "indexOf", "(Ljava/lang/String;)I")
		return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && types[1] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "indexOf", "(Ljava/lang/String;I)I")
		return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Java_lang_StringInstance) Intern() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "intern", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Java_lang_StringInstance) IsEmpty() (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "isEmpty", "()Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Java_lang_StringClass) Java_lang_String(args ...interface{}) (*Java_lang_StringInstance, error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return nil, err
	}
	switch {
	case len(args) == 0:
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "()V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id)
		if err != nil {
			return nil, err
		}
		return Java_lang_String_Instance(e.vmc, obj), nil

	case len(args) == 1 && strings.HasPrefix(types[0], "java/lang/String"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "(Ljava/lang/String;)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))))
		if err != nil {
			return nil, err
		}
		return Java_lang_String_Instance(e.vmc, obj), nil

	case len(args) == 1 && strings.HasPrefix(types[0], "[C"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "([C)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))))
		if err != nil {
			return nil, err
		}
		return Java_lang_String_Instance(e.vmc, obj), nil

	case len(args) == 3 && strings.HasPrefix(types[0], "[C") && types[1] == "int32" && types[2] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "([CII)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))), jni.Value((args[2].(int32))))
		if err != nil {
			return nil, err
		}
		return Java_lang_String_Instance(e.vmc, obj), nil

	case len(args) == 3 && strings.HasPrefix(types[0], "[I") && types[1] == "int32" && types[2] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "([III)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))), jni.Value((args[2].(int32))))
		if err != nil {
			return nil, err
		}
		return Java_lang_String_Instance(e.vmc, obj), nil

	case len(args) == 4 && strings.HasPrefix(types[0], "[B") && types[1] == "int32" && types[2] == "int32" && types[3] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "([BIII)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))), jni.Value((args[2].(int32))), jni.Value((args[3].(int32))))
		if err != nil {
			return nil, err
		}
		return Java_lang_String_Instance(e.vmc, obj), nil

	case len(args) == 2 && strings.HasPrefix(types[0], "[B") && types[1] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "([BI)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))))
		if err != nil {
			return nil, err
		}
		return Java_lang_String_Instance(e.vmc, obj), nil

	case len(args) == 4 && strings.HasPrefix(types[0], "[B") && types[1] == "int32" && types[2] == "int32" && strings.HasPrefix(types[3], "java/lang/String"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "([BIILjava/lang/String;)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))), jni.Value((args[2].(int32))), jni.Value(ObjVal(args[3].(jni.Object))))
		if err != nil {
			return nil, err
		}
		return Java_lang_String_Instance(e.vmc, obj), nil

	case len(args) == 4 && strings.HasPrefix(types[0], "[B") && types[1] == "int32" && types[2] == "int32" && strings.HasPrefix(types[3], "java/nio/charset/Charset"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "([BIILjava/nio/charset/Charset;)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))), jni.Value((args[2].(int32))), jni.Value(ObjVal(args[3].(jni.Object))))
		if err != nil {
			return nil, err
		}
		return Java_lang_String_Instance(e.vmc, obj), nil

	case len(args) == 2 && strings.HasPrefix(types[0], "[B") && strings.HasPrefix(types[1], "java/lang/String"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "([BLjava/lang/String;)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
		if err != nil {
			return nil, err
		}
		return Java_lang_String_Instance(e.vmc, obj), nil

	case len(args) == 2 && strings.HasPrefix(types[0], "[B") && strings.HasPrefix(types[1], "java/nio/charset/Charset"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "([BLjava/nio/charset/Charset;)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
		if err != nil {
			return nil, err
		}
		return Java_lang_String_Instance(e.vmc, obj), nil

	case len(args) == 3 && strings.HasPrefix(types[0], "[B") && types[1] == "int32" && types[2] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "([BII)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))), jni.Value((args[2].(int32))))
		if err != nil {
			return nil, err
		}
		return Java_lang_String_Instance(e.vmc, obj), nil

	case len(args) == 1 && strings.HasPrefix(types[0], "[B"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "([B)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))))
		if err != nil {
			return nil, err
		}
		return Java_lang_String_Instance(e.vmc, obj), nil

	case len(args) == 1 && strings.HasPrefix(types[0], "java/lang/StringBuffer"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "(Ljava/lang/StringBuffer;)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))))
		if err != nil {
			return nil, err
		}
		return Java_lang_String_Instance(e.vmc, obj), nil

	case len(args) == 1 && strings.HasPrefix(types[0], "java/lang/StringBuilder"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "(Ljava/lang/StringBuilder;)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))))
		if err != nil {
			return nil, err
		}
		return Java_lang_String_Instance(e.vmc, obj), nil
default:
		return nil, overloadError(types)
	}
}

func (e *Java_lang_StringClass) Join(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/CharSequence") && strings.HasPrefix(types[1], "[Ljava/lang/CharSequence;"):
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "join", "(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/String;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/CharSequence") && strings.HasPrefix(types[1], "java/lang/Iterable"):
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "join", "(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Java_lang_StringInstance) LastIndexOf(args ...interface{}) (ret int32, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 1 && types[0] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "lastIndexOf", "(I)I")
		return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((args[0].(int32))))
	case len(args) == 2 && types[0] == "int32" && types[1] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "lastIndexOf", "(II)I")
		return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((args[0].(int32))), jni.Value((args[1].(int32))))
	case len(args) == 1 && strings.HasPrefix(types[0], "java/lang/String"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "lastIndexOf", "(Ljava/lang/String;)I")
		return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && types[1] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "lastIndexOf", "(Ljava/lang/String;I)I")
		return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Java_lang_StringInstance) Length() (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "length", "()I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Java_lang_StringInstance) Matches(arg0 jni.Object) (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "matches", "(Ljava/lang/String;)Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Java_lang_StringInstance) OffsetByCodePoints(arg0 int32, arg1 int32) (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "offsetByCodePoints", "(II)I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((arg0)), jni.Value((arg1)))
}

func (e *Java_lang_StringInstance) RegionMatches(args ...interface{}) (ret bool, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 4 && types[0] == "int32" && strings.HasPrefix(types[1], "java/lang/String") && types[2] == "int32" && types[3] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "regionMatches", "(ILjava/lang/String;II)Z")
		return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((args[0].(int32))), jni.Value(ObjVal(args[1].(jni.Object))), jni.Value((args[2].(int32))), jni.Value((args[3].(int32))))
	case len(args) == 5 && types[0] == "bool" && types[1] == "int32" && strings.HasPrefix(types[2], "java/lang/String") && types[3] == "int32" && types[4] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "regionMatches", "(ZILjava/lang/String;II)Z")
		return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(Boole(args[0].(bool))), jni.Value((args[1].(int32))), jni.Value(ObjVal(args[2].(jni.Object))), jni.Value((args[3].(int32))), jni.Value((args[4].(int32))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Java_lang_StringInstance) Replace(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 2 && types[0] == "rune" && types[1] == "rune":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "replace", "(CC)Ljava/lang/String;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((args[0].(rune))), jni.Value((args[1].(rune))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/CharSequence") && strings.HasPrefix(types[1], "java/lang/CharSequence"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "replace", "(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Java_lang_StringInstance) ReplaceAll(arg0 jni.Object, arg1 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "replaceAll", "(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)))
}

func (e *Java_lang_StringInstance) ReplaceFirst(arg0 jni.Object, arg1 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "replaceFirst", "(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)))
}

func (e *Java_lang_StringInstance) Split(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && types[1] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "split", "(Ljava/lang/String;I)[Ljava/lang/String;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))))
	case len(args) == 1 && strings.HasPrefix(types[0], "java/lang/String"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "split", "(Ljava/lang/String;)[Ljava/lang/String;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Java_lang_StringInstance) StartsWith(args ...interface{}) (ret bool, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && types[1] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "startsWith", "(Ljava/lang/String;I)Z")
		return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))))
	case len(args) == 1 && strings.HasPrefix(types[0], "java/lang/String"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "startsWith", "(Ljava/lang/String;)Z")
		return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Java_lang_StringInstance) SubSequence(arg0 int32, arg1 int32) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "subSequence", "(II)Ljava/lang/CharSequence;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((arg0)), jni.Value((arg1)))
}

func (e *Java_lang_StringInstance) Substring(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 1 && types[0] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "substring", "(I)Ljava/lang/String;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((args[0].(int32))))
	case len(args) == 2 && types[0] == "int32" && types[1] == "int32":
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "substring", "(II)Ljava/lang/String;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value((args[0].(int32))), jni.Value((args[1].(int32))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Java_lang_StringInstance) ToCharArray() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "toCharArray", "()[C")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Java_lang_StringInstance) ToLowerCase(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 1 && strings.HasPrefix(types[0], "java/util/Locale"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "toLowerCase", "(Ljava/util/Locale;)Ljava/lang/String;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 0:
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "toLowerCase", "()Ljava/lang/String;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
	default:
		err = overloadError(types)
		return
	}
}

func (e *Java_lang_StringInstance) ToString() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "toString", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Java_lang_StringInstance) ToUpperCase(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 1 && strings.HasPrefix(types[0], "java/util/Locale"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "toUpperCase", "(Ljava/util/Locale;)Ljava/lang/String;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 0:
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "toUpperCase", "()Ljava/lang/String;")
		return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
	default:
		err = overloadError(types)
		return
	}
}

func (e *Java_lang_StringInstance) Trim() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "trim", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Java_lang_StringClass) ValueOf(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 1 && strings.HasPrefix(types[0], "java/lang/Object"):
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "valueOf", "(Ljava/lang/Object;)Ljava/lang/String;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 1 && strings.HasPrefix(types[0], "[C"):
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "valueOf", "([C)Ljava/lang/String;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 3 && strings.HasPrefix(types[0], "[C") && types[1] == "int32" && types[2] == "int32":
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "valueOf", "([CII)Ljava/lang/String;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value((args[1].(int32))), jni.Value((args[2].(int32))))
	case len(args) == 1 && types[0] == "bool":
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "valueOf", "(Z)Ljava/lang/String;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(Boole(args[0].(bool))))
	case len(args) == 1 && types[0] == "rune":
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "valueOf", "(C)Ljava/lang/String;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((args[0].(rune))))
	case len(args) == 1 && types[0] == "int32":
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "valueOf", "(I)Ljava/lang/String;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((args[0].(int32))))
	case len(args) == 1 && types[0] == "int64":
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "valueOf", "(J)Ljava/lang/String;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value((args[0].(int64))))
	case len(args) == 1 && types[0] == "float32":
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "valueOf", "(F)Ljava/lang/String;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(math.Float32bits(args[0].(float32))))
	case len(args) == 1 && types[0] == "float64":
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "valueOf", "(D)Ljava/lang/String;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(math.Float64bits(args[0].(float64))))
	default:
		err = overloadError(types)
		return
	}
}

