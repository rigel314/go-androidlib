package androidlib

import (
	"git.wow.st/gmp/jni"
)

type VMContext struct {
	JNIEnv jni.Env
}

func NewVMContext(jnienv uintptr) *VMContext {
	return &VMContext{
		JNIEnv: jni.EnvFor(jnienv),
	}
}
