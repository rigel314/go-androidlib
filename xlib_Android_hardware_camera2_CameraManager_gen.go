package androidlib

import (
	"math"
	"strings"

	"git.wow.st/gmp/jni"
)

var _ = math.Float32bits
var _ = strings.HasPrefix

type Android_hardware_camera2_CameraManagerClass struct {
	vmc   *VMContext
	Class jni.Class
}

type Android_hardware_camera2_CameraManagerInstance struct {
	*Android_hardware_camera2_CameraManagerClass
	Jobj jni.Object
}

func Android_hardware_camera2_CameraManager(vmc *VMContext) *Android_hardware_camera2_CameraManagerClass {
	return &Android_hardware_camera2_CameraManagerClass{
		vmc:   vmc,
		Class: jni.FindClass(vmc.JNIEnv, "android/hardware/camera2/CameraManager"),
	}
}

func Android_hardware_camera2_CameraManager_Instance(vmc *VMContext, jobj jni.Object) *Android_hardware_camera2_CameraManagerInstance {
	return &Android_hardware_camera2_CameraManagerInstance{
		Android_hardware_camera2_CameraManagerClass: Android_hardware_camera2_CameraManager(vmc),
		Jobj: jobj,
	}
}

func (e *Android_hardware_camera2_CameraManagerInstance) GetCameraCharacteristics(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getCameraCharacteristics", "(Ljava/lang/String;)Landroid/hardware/camera2/CameraCharacteristics;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_hardware_camera2_CameraManagerInstance) GetCameraExtensionCharacteristics(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getCameraExtensionCharacteristics", "(Ljava/lang/String;)Landroid/hardware/camera2/CameraExtensionCharacteristics;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_hardware_camera2_CameraManagerInstance) GetCameraIdList() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getCameraIdList", "()[Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_hardware_camera2_CameraManagerInstance) GetConcurrentCameraIds() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getConcurrentCameraIds", "()Ljava/util/Set;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_hardware_camera2_CameraManagerInstance) IsConcurrentSessionConfigurationSupported(arg0 jni.Object) (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "isConcurrentSessionConfigurationSupported", "(Ljava/util/Map;)Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_hardware_camera2_CameraManagerInstance) OpenCamera(args ...interface{}) (err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 3 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "android/hardware/camera2/CameraDevice$StateCallback") && strings.HasPrefix(types[2], "android/os/Handler"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "openCamera", "(Ljava/lang/String;Landroid/hardware/camera2/CameraDevice$StateCallback;Landroid/os/Handler;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))), jni.Value(ObjVal(args[2].(jni.Object))))
	case len(args) == 3 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "java/util/concurrent/Executor") && strings.HasPrefix(types[2], "android/hardware/camera2/CameraDevice$StateCallback"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "openCamera", "(Ljava/lang/String;Ljava/util/concurrent/Executor;Landroid/hardware/camera2/CameraDevice$StateCallback;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))), jni.Value(ObjVal(args[2].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_hardware_camera2_CameraManagerInstance) RegisterAvailabilityCallback(args ...interface{}) (err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 2 && strings.HasPrefix(types[0], "android/hardware/camera2/CameraManager$AvailabilityCallback") && strings.HasPrefix(types[1], "android/os/Handler"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "registerAvailabilityCallback", "(Landroid/hardware/camera2/CameraManager$AvailabilityCallback;Landroid/os/Handler;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/util/concurrent/Executor") && strings.HasPrefix(types[1], "android/hardware/camera2/CameraManager$AvailabilityCallback"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "registerAvailabilityCallback", "(Ljava/util/concurrent/Executor;Landroid/hardware/camera2/CameraManager$AvailabilityCallback;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_hardware_camera2_CameraManagerInstance) RegisterTorchCallback(args ...interface{}) (err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 2 && strings.HasPrefix(types[0], "android/hardware/camera2/CameraManager$TorchCallback") && strings.HasPrefix(types[1], "android/os/Handler"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "registerTorchCallback", "(Landroid/hardware/camera2/CameraManager$TorchCallback;Landroid/os/Handler;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "java/util/concurrent/Executor") && strings.HasPrefix(types[1], "android/hardware/camera2/CameraManager$TorchCallback"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "registerTorchCallback", "(Ljava/util/concurrent/Executor;Landroid/hardware/camera2/CameraManager$TorchCallback;)V")
		return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_hardware_camera2_CameraManagerInstance) SetTorchMode(arg0 jni.Object, arg1 bool) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "setTorchMode", "(Ljava/lang/String;Z)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value(Boole(arg1)))
}

func (e *Android_hardware_camera2_CameraManagerInstance) UnregisterAvailabilityCallback(arg0 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "unregisterAvailabilityCallback", "(Landroid/hardware/camera2/CameraManager$AvailabilityCallback;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_hardware_camera2_CameraManagerInstance) UnregisterTorchCallback(arg0 jni.Object) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "unregisterTorchCallback", "(Landroid/hardware/camera2/CameraManager$TorchCallback;)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

