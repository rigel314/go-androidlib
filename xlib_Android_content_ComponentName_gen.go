package androidlib

import (
	"math"
	"strings"

	"git.wow.st/gmp/jni"
)

var _ = math.Float32bits
var _ = strings.HasPrefix

type Android_content_ComponentNameClass struct {
	vmc   *VMContext
	Class jni.Class
}

type Android_content_ComponentNameInstance struct {
	*Android_content_ComponentNameClass
	Jobj jni.Object
}

func Android_content_ComponentName(vmc *VMContext) *Android_content_ComponentNameClass {
	return &Android_content_ComponentNameClass{
		vmc:   vmc,
		Class: jni.FindClass(vmc.JNIEnv, "android/content/ComponentName"),
	}
}

func Android_content_ComponentName_Instance(vmc *VMContext, jobj jni.Object) *Android_content_ComponentNameInstance {
	return &Android_content_ComponentNameInstance{
		Android_content_ComponentNameClass: Android_content_ComponentName(vmc),
		Jobj: jobj,
	}
}

func (e *Android_content_ComponentNameClass) CREATOR() jni.Object {
	id := jni.GetStaticFieldID(e.vmc.JNIEnv, e.Class, "CREATOR", "Landroid/os/Parcelable$Creator;")
	return jni.GetStaticObjectField(e.vmc.JNIEnv, e.Class, id)
}

func (e *Android_content_ComponentNameClass) Android_content_ComponentName(args ...interface{}) (*Android_content_ComponentNameInstance, error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return nil, err
	}
	switch {
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "java/lang/String"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "(Ljava/lang/String;Ljava/lang/String;)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
		if err != nil {
			return nil, err
		}
		return Android_content_ComponentName_Instance(e.vmc, obj), nil

	case len(args) == 2 && strings.HasPrefix(types[0], "android/content/Context") && strings.HasPrefix(types[1], "java/lang/String"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "(Landroid/content/Context;Ljava/lang/String;)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
		if err != nil {
			return nil, err
		}
		return Android_content_ComponentName_Instance(e.vmc, obj), nil

	case len(args) == 2 && strings.HasPrefix(types[0], "android/content/Context") && strings.HasPrefix(types[1], "java/lang/Class"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "(Landroid/content/Context;Ljava/lang/Class;)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
		if err != nil {
			return nil, err
		}
		return Android_content_ComponentName_Instance(e.vmc, obj), nil

	case len(args) == 1 && strings.HasPrefix(types[0], "android/os/Parcel"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "<init>", "(Landroid/os/Parcel;)V")
		obj, err := jni.NewObject(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))))
		if err != nil {
			return nil, err
		}
		return Android_content_ComponentName_Instance(e.vmc, obj), nil
default:
		return nil, overloadError(types)
	}
}

func (e *Android_content_ComponentNameInstance) Clone() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "clone", "()Landroid/content/ComponentName;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ComponentNameInstance) CompareTo(args ...interface{}) (ret int32, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 1 && strings.HasPrefix(types[0], "android/content/ComponentName"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "compareTo", "(Landroid/content/ComponentName;)I")
		return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	case len(args) == 1 && strings.HasPrefix(types[0], "java/lang/Object"):
		id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "compareTo", "(Ljava/lang/Object;)I")
		return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(args[0].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_ComponentNameClass) CreateRelative(args ...interface{}) (ret jni.Object, err error) {
	types, err := ArgTypes(e.vmc, args...)
	if err != nil {
		return
	}
	switch {
	case len(args) == 2 && strings.HasPrefix(types[0], "java/lang/String") && strings.HasPrefix(types[1], "java/lang/String"):
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "createRelative", "(Ljava/lang/String;Ljava/lang/String;)Landroid/content/ComponentName;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	case len(args) == 2 && strings.HasPrefix(types[0], "android/content/Context") && strings.HasPrefix(types[1], "java/lang/String"):
		id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "createRelative", "(Landroid/content/Context;Ljava/lang/String;)Landroid/content/ComponentName;")
		return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(args[0].(jni.Object))), jni.Value(ObjVal(args[1].(jni.Object))))
	default:
		err = overloadError(types)
		return
	}
}

func (e *Android_content_ComponentNameInstance) DescribeContents() (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "describeContents", "()I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ComponentNameInstance) Equals(arg0 jni.Object) (ret bool, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "equals", "(Ljava/lang/Object;)Z")
	return jni.CallBooleanMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ComponentNameInstance) FlattenToShortString() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "flattenToShortString", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ComponentNameInstance) FlattenToString() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "flattenToString", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ComponentNameInstance) GetClassName() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getClassName", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ComponentNameInstance) GetPackageName() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getPackageName", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ComponentNameInstance) GetShortClassName() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "getShortClassName", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ComponentNameInstance) HashCode() (ret int32, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "hashCode", "()I")
	return jni.CallIntMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ComponentNameClass) ReadFromParcel(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "readFromParcel", "(Landroid/os/Parcel;)Landroid/content/ComponentName;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ComponentNameInstance) ToShortString() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "toShortString", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ComponentNameInstance) ToString() (ret jni.Object, err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "toString", "()Ljava/lang/String;")
	return jni.CallObjectMethod(e.vmc.JNIEnv, e.Jobj, id)
}

func (e *Android_content_ComponentNameClass) UnflattenFromString(arg0 jni.Object) (ret jni.Object, err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "unflattenFromString", "(Ljava/lang/String;)Landroid/content/ComponentName;")
	return jni.CallStaticObjectMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(arg0)))
}

func (e *Android_content_ComponentNameClass) WriteToParcel(arg0 jni.Object, arg1 jni.Object) (err error) {
	id := jni.GetStaticMethodID(e.vmc.JNIEnv, e.Class, "writeToParcel", "(Landroid/content/ComponentName;Landroid/os/Parcel;)V")
	return jni.CallStaticVoidMethod(e.vmc.JNIEnv, e.Class, id, jni.Value(ObjVal(arg0)), jni.Value(ObjVal(arg1)))
}

func (e *Android_content_ComponentNameInstance) WriteToParcel(arg0 jni.Object, arg1 int32) (err error) {
	id := jni.GetMethodID(e.vmc.JNIEnv, e.Class, "writeToParcel", "(Landroid/os/Parcel;I)V")
	return jni.CallVoidMethod(e.vmc.JNIEnv, e.Jobj, id, jni.Value(ObjVal(arg0)), jni.Value((arg1)))
}

